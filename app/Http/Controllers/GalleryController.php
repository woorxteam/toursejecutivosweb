<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;
use App\Category;
use App\Album;
use App\Photo;
use Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = Album::albums();
        $data = array(
            'galleries'   => $galleries
        );

        return view('gallery.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::list_categories('gallery');

        $data = array(
            'categories'    => $categories
        );

        return view('gallery.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'title.required'              => 'El campo título es obligatorio.',
            'document_name.required'      => 'La imagen es obligatoria.'
        ];

        $validator = Validator::make($request->all(), [
            'title'              => 'required',
            'document_name'      => 'required'
        ], $messages);

        if ($validator->fails()) {
            return redirect('gallery/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $gallery  = $request->all();

        try {
            \DB::transaction(function() use ($gallery) {

                $row = new Album;
                $row->title            = $gallery['title'];
                $row->image            = $gallery['document_name'];
                $row->status           = $gallery['status'];
                $row->save();

                $gallery_id = $row->id;

                \Session::flash('gallery_id', $gallery_id);

            });

        } catch (\ErrorException $e) {
            \Session::flash('add_errors','error');
            return redirect('/gallery/create')->with('add_errors', true);
        }

        return redirect('/gallery/' . \Session::get('gallery_id') . '/photos')->with('status', 'Galería creada con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $galleries = Photo::galleries($id);
        $album = Album::find($id);

        $data = array(
            'galleries'    => $galleries,
            'album'        => $album
        );
        return view('gallery.photos', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $album = Album::find($id);
        $data = array(
            'album'       => $album
        );

        return view('gallery.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'title.required'              => 'El campo título es obligatorio.'
        ];

        $validator = Validator::make($request->all(), [
            'title'              => 'required'
        ], $messages);

        if ($validator->fails()) {
            return redirect('gallery/' . $id . '/edit')
                        ->withErrors($validator)
                        ->withInput();
        }

        $gallery  = $request->all();

        try {
            \DB::transaction(function() use ($gallery, $id) {

                $row = Album::find($id);
                $row->title            = $gallery['title'];
                $row->status           = $gallery['status'];

                if($gallery['document_name'] != '')
                    $row->image = $gallery['document_name'];

                $row->save();
            });

        } catch (\ErrorException $e) {
            \Session::flash('add_errors','error');
            return redirect('/gallery/' . $id . '/edit')->with('add_errors', true);
        }

        return redirect('/gallery')->with('status', 'Galería modificada con éxito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
          $row = Album::find($id);
          $row->status = 'deleted';
          $row->save();
        } catch (\ErrorException $e) {
            \Session::flash('add_errors','error');
            return redirect('/gallery')->with('delete_errors', true);
        }

        return redirect('/gallery')->with('status', 'Galería eliminada con éxito!');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyPhoto($album_id, $photo_id)
    {
        try {
          $row = Photo::galleryDetail($photo_id);
          Photo::destroy($photo_id);

          \File::Delete(public_path().'/images/' . $row->creacion . '/' . $row->photo);

        } catch (\ErrorException $e) {
            \Session::flash('add_errors','error');
            return redirect('/gallery/'.$album_id.'/detail')->with('delete_errors', true);
        }

        return redirect('/gallery/'.$album_id.'/detail')->with('status', 'Foto eliminada con éxito!');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createPhotos($id)
    {
        $album = Album::find($id);
        $data = array(
            'album'     => $album
        );

        return view('gallery.create_photos', $data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePhotos(Request $request, $id)
    {
      $messages = [
        'file.required'         => 'La documentación es obligatoria.',
        'file.mimes'            => 'Solo se permiten archivos tipo JPG.'
      ];

      $validator = Validator::make($request->all(), [
          'file'      => 'required|mimes:jpeg,png'
      ], $messages);

      if ($validator->fails()) {
          return response()->json(array('exito' => false), 400);
      } else {
          $document = $request->all();

          $file = $request->file('file');
          $documentation = "";

          if ($file != null) {
              $size = $file->getClientSize();
              $extension = $file->getClientOriginalExtension();
              $documentation = date('Y-m-d H:i:s'). '_' . $file->getClientOriginalName();
              $maximo = 2;

              if ($size > 0) {
                  if (($size / 1000000) > $maximo){
                      return response()->json(array('exito' => false), 400);
                  }
              } else {
                  return response()->json(array('exito' => false), 200);
              }

              $album = Album::detail($id);
              $fecha = $album->creacion;

              \Storage::disk('image')->put($fecha . '/' . $documentation,  \File::get($file));

              $row = new Photo;
              $row->photo            = $documentation;
              $row->album_id         = $id;
              $row->save();

              return response()->json(array('exito' => true, 'file' => $documentation), 200);
          }
      }

      return response()->json(array('exito' => false), 400);
    }


    public function upload(Request $request)
    {
        $messages = [
          'documentation.required'         => 'La documentación es obligatoria.',
          'documentation.mimes'            => 'Solo se permiten archivos tipo JPG.'
        ];

        $validator = Validator::make($request->all(), [
            'documentation'      => 'required|mimes:jpeg,png'
        ], $messages);

        if ($validator->fails()) {
            return response()->json(array('exito' => false), 200);
        } else {
            $document = $request->all();

            $file = $request->file('documentation');
            $documentation = "";

            if ($file != null) {
                $size = $file->getClientSize();
                $extension = $file->getClientOriginalExtension();
                $documentation = md5(time()) . "." . $extension;
                $maximo = 2;

                if ($size > 0) {
                    if (($size / 1000000) > $maximo){
                        return response()->json(array('exito' => false), 200);
                    }
                } else {
                    return response()->json(array('exito' => false), 200);
                }

                if ($document['date'] != '') {
                    $fecha = $document['date'];
                }  else {
                    $fecha = date('m'). '_' . date('Y');
                }

                \Storage::disk('image')->put($fecha . '/' . $documentation,  \File::get($file));

                return response()->json(array('exito' => true, 'file' => $documentation), 200);
            }
        }

        return response()->json(array('exito' => false), 200);
    }
}
