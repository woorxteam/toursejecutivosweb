<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Publication;
use App\Category;
use Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str as Str;

class PublicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publications = Publication::publications();
        $data = array(
            'publications'    => $publications
        );

        return view('publications.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::list_categories('publication');

        $data = array(
            'categories'    => $categories
        );

        return view('publications.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
          'title.required'              => 'El campo título es obligatorio.',
          'title.unique'                => 'El campo título ya ha sido registrado.',
          'subtitle.required'           => 'El campo subtítulo es obligatorio.',
          'description.required'        => 'El contenido de la publicación es obligatorio.',
          'status.required'             => 'El campo status es obligatorio.',
          'type.required'               => 'El campo tipo de publicación es obligatorio.',
          'category.required'           => 'El campo categoría es obligatorio.',
          'category.exists'             => 'El campo categoría no es válido.',
          'category.integer'            => 'El campo categoría no es válido.',
          'document_name.required_if'   => 'La imagen es obligatoria.'
        ];

        $validator = Validator::make($request->all(), [
            'title'              => 'required|unique:publications,title',
            'subtitle'           => 'required',
            'description'        => 'required',
            'status'             => 'required',
            'type'               => 'required',
            'category'           => 'required|integer|exists:categories,id',
            'document_name'      => 'required_if:type,content'
        ], $messages);

        if ($validator->fails()) {
            return redirect('publications/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $publication  = $request->all();

        try {
            \DB::transaction(function() use ($publication) {

                $row = new Publication;
                $row->title            = $publication['title'];
                $row->slug             = Str::slug($publication['title']);
                $row->subtitle         = $publication['subtitle'];
                $row->description      = $publication['description'];
                $row->image            = $publication['document_name'];
                $row->category_id      = $publication['category'];
                $row->status           = $publication['status'];
                $row->created_by       = \Auth::user()->id;
                $row->type             = $publication['type'];
                $row->save();

            });

        } catch (\ErrorException $e) {
            \Session::flash('add_errors','error');
            return redirect('/publications/create')->with('add_errors', true);
        }

        return redirect("/publications")->with('status', 'Publicación creada con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $publication = Publication::find($id);
        $categories = Category::list_categories('publication');
        $data = array(
            'publication'       => $publication,
            'categories'        => $categories
        );

        return view('publications.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $messages = [
        'title.required'              => 'El campo título es obligatorio.',
        'title.unique'                => 'El campo título ya ha sido registrado.',
        'subtitle.required'           => 'El campo subtítulo es obligatorio.',
        'description.required'        => 'El contenido de la publicación es obligatorio.',
        'status.required'             => 'El campo status es obligatorio.',
        'category.required'           => 'El campo categoría es obligatorio.',
        'category.exists'             => 'El campo categoría no es válido.',
        'category.integer'            => 'El campo categoría no es válido.',
        'type.required'               => 'El campo tipo de publicación es obligatorio.'
      ];

      $validator = Validator::make($request->all(), [
          'title'              => 'required|unique:publications,title,' . $id,
          'subtitle'           => 'required',
          'description'        => 'required',
          'status'             => 'required',
          'category'           => 'required|integer|exists:categories,id',
          'type'               => 'required'
      ], $messages);

      if ($validator->fails()) {
          return redirect('publications/' . $id . '/edit')
                      ->withErrors($validator)
                      ->withInput();
      }

      $publication  = $request->all();

      try {
          \DB::transaction(function() use ($publication, $id) {

              $row = Publication::find($id);
              $row->title            = $publication['title'];
              $row->slug             = Str::slug($publication['title']);
              $row->subtitle         = $publication['subtitle'];
              $row->description      = $publication['description'];
              $row->status           = $publication['status'];
              $row->type             = $publication['type'];

              if($publication['document_name'] != "")
                  $row->image = $publication['document_name'];


              $row->save();

          });

      } catch (\ErrorException $e) {
          \Session::flash('add_errors','error');
          return redirect('/publications/' . $id . '/edit')->with('add_errors', true);
      }

      return redirect("/publications")->with('status', 'Publicación creada con éxito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
          $row = Publication::find($id);
          $row->status = 'deleted';
          $row->save();
        } catch (\ErrorException $e) {
            \Session::flash('add_errors','error');
            return redirect('/publications')->with('delete_errors', true);
        }

        return redirect('/publications')->with('status', 'Publicación eliminada con éxito!');
    }



    public function upload(Request $request)
    {
        $messages = [
          'documentation.required'         => 'La documentación es obligatoria.',
          'documentation.mimes'            => 'Solo se permiten archivos tipo JPG.'
        ];

        $validator = Validator::make($request->all(), [
            'documentation'      => 'required|mimes:jpeg,png'
        ], $messages);

        if ($validator->fails()) {
            return response()->json(array('exito' => false), 200);
        } else {
            $document = $request->all();

            $file = $request->file('documentation');
            $documentation = "";

            if ($file != null) {
                $size = $file->getClientSize();
                $extension = $file->getClientOriginalExtension();
                $documentation = md5(time()) . "." . $extension;
                $maximo = 2;

                if ($size > 0) {
                    if (($size / 1000000) > $maximo){
                        return response()->json(array('exito' => false), 200);
                    }
                } else {
                    return response()->json(array('exito' => false), 200);
                }

                if ($document['date'] != '') {
                    $fecha = $document['date'];
                }  else {
                    $fecha = date('m'). '_' . date('Y');
                }

                \Storage::disk('image')->put($fecha . '/' . $documentation,  \File::get($file));

                return response()->json(array('exito' => true, 'file' => $documentation), 200);
            }
        }

        return response()->json(array('exito' => false), 200);
    }
}
