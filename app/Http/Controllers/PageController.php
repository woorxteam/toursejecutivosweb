<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Publication;
use App\Gallery;
use App\Category;
use App\Album;
use App\Photo;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
class PageController extends Controller
{
  protected $language, $cookie;

  public function __construct (Request $request){

    $this->cookie = $request->cookie('lang');
    $this->language = $request->input('lang');

    if (!$this->cookie && $this->language){
      $this->cookie= cookie('lang', $this->language, 525600);
    }elseif ($this->cookie && !$this->language) {
      $this->language = $this->cookie;
    }elseif (!$this->cookie && !$this->language) {
      $this->language = 'es';
      $this->cookie= cookie('lang', 'es', 525600);
    }else{
      $this->cookie= cookie('lang', $this->language, 525600);
    }
    \App::SetLocale($this->language);
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $view = \View::make('page.index');
      return \Response::make($view)->withCookie($this->cookie);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function acerca()
    {
      $view = \View::make('page.acerca');
      return \Response::make($view)->withCookie($this->cookie);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contacto()
    {
        $view = \View::make('page.contacto');
        return \Response::make($view)->withCookie($this->cookie);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tiida()
    {
        $view = \View::make('page.tiida');
        return \Response::make($view)->withCookie($this->cookie);
    }


    public function send(Request $request)
    {
        $messages = [
            'name.required'       => 'El campo nombre es obligatorio.',
            'email.required'      => 'El campo email es obligatorio.',
            'email.email'         => 'El campo email no es válido.',
        ];

        $validator = Validator::make($request->all(), [
            'name'               => 'required',
            'email'              => 'required|email'
        ], $messages);

        if ($validator->fails()) {
            return redirect('/contacto')
                        ->withErrors($validator)
                        ->withInput();
        }

        $contact  = $request->all();

        try {

          //---->Enviamos la cotización al contacto
          \Mail::send('emails.contact', $contact, function($message) use ($contact)
          {
               //remitente
               $message->from("alejandro.delpiero@gmail.com", "Tours Ejecutivos");

               //asunto
               $message->subject("Contacto desde el sitio web Tours ejecutivos");

               //receptor
               $message->to("alex@woorx.mx", "Tours Ejecutivos");
          });
        } catch (\ErrorException $e) {
            dd($e);
            \Session::flash('add_errors','error');
            return redirect("/")->with('add_errors', true);
        }

        return redirect("/");

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crafter()
    {
        $view = \View::make('page.crafter');
        return \Response::make($view)->withCookie($this->cookie);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function volksbus()
    {
        $view = \View::make('page.volksbus');
        return \Response::make($view)->withCookie($this->cookie);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function urvan()
    {
        $view = \View::make('page.urvan');
        return \Response::make($view)->withCookie($this->cookie);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function camion()
    {
        $view = \View::make('page.camion');
        return \Response::make($view)->withCookie($this->cookie);
    }

    /**
 * Show the form for creating a new resource.
 *
 * @return \Illuminate\Http\Response
 */
    public function escalade()
    {
        $view = \View::make('page.escalade');
        return \Response::make($view)->withCookie($this->cookie);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function passat()
    {
        $view = \View::make('page.passat');
        return \Response::make($view)->withCookie($this->cookie);
    }

    public function suburban()
    {
        $view = \View::make('page.suburban');
        return \Response::make($view)->withCookie($this->cookie);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function galeria()
    {
        $albums = Album::albumsPage();
        $data = array(
            'albums'  => $albums
        );

        return view('page.galeria', $data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detallelGaleria($album_id)
    {
        $galleries = Photo::galleries($album_id);
        $album = Album::find($album_id);

        $data = array(
            'galleries'   => $galleries,
            'album'       => $album
        );

        return view('page.detalle_galeria', $data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function blog()
    {
        $publications = Publication::publications_blog();
        $data = array(
            'publications'    => $publications
        );

        return view('page.blog', $data);
    }


    public function show($slug)
    {
        $post = Publication::publication_detail($slug);

        if (isset($post->id)) {
            $data = array(
                'publication'    => $post
            );

            return view('page.detail', $data);
        }

        return view('error');
    }

}
