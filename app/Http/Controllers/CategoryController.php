<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $categories = Category::categories();
      $data = array(
          'categories'    => $categories
      );

      return view('categories.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
          'name.required'               => 'El campo nombre es obligatorio.',
          'name.unique'                 => 'El valor del campo nombre ya ha sido registrado.',
          'status.required'             => 'El campo status es obligatorio.',
          'type.required'               => 'El tipo de categoría es requerido'
        ];

        $validator = Validator::make($request->all(), [
            'name'              => 'required|unique:categories,name',
            'status'            => 'required',
            'type'              => 'required'
        ], $messages);

        if ($validator->fails()) {
            return redirect('categories/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $category  = $request->all();

        try {
            \DB::transaction(function() use ($category) {

                $row = new Category;
                $row->name             = $category['name'];
                $row->type             = $category['type'];
                $row->status           = $category['status'];
                $row->image            = $category['document_name'];
                $row->save();
            });

        } catch (\ErrorException $e) {
            \Session::flash('add_errors','error');
            return redirect('/categories/create')->with('add_errors', true);
        }

        return redirect("/categories")->with('status', 'Categoría creada con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $category = Category::find($id);
      $data = array(
          'category'       => $category
      );

      return view('categories.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
          'name.required'               => 'El campo nombre es obligatorio.',
          'name.unique'                 => 'El valor del campo nombre ya ha sido registrado.',
          'status.required'             => 'El campo status es obligatorio.',
          'type.required'               => 'El tipo de categoría es obligatorio.',
        ];

        $validator = Validator::make($request->all(), [
            'name'              => 'required|unique:categories,name,' . $id,
            'status'            => 'required',
            'type'              => 'required'
        ], $messages);

        if ($validator->fails()) {
            return redirect('categories/' . $id . '/edit')
                        ->withErrors($validator)
                        ->withInput();
        }

        $category  = $request->all();

        try {
            \DB::transaction(function() use ($category, $id) {

                $row = Category::find($id);
                $row->name             = $category['name'];
                $row->status           = $category['status'];
                $row->type             = $category['type'];

                if ($category['document_name'] != "")
                    $row->image = $category['document_name'];

                $row->save();
            });

        } catch (\ErrorException $e) {
            \Session::flash('add_errors','error');
            return redirect('/categories/' . $id . '/edit')->with('add_errors', true);
        }

        return redirect("/categories")->with('status', 'Categoría modificada con éxito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
          $row = Category::find($id);
          $row->status = 'deleted';
          $row->save();
        } catch (\ErrorException $e) {
            \Session::flash('add_errors','error');
            return redirect('/categories')->with('delete_errors', true);
        }

        return redirect('/categories')->with('status', 'Categoría eliminada con éxito!');
    }



    public function upload(Request $request)
    {
        $messages = [
          'documentation.required'         => 'La documentación es obligatoria.',
          'documentation.mimes'            => 'Solo se permiten archivos tipo JPG.'
        ];

        $validator = Validator::make($request->all(), [
            'documentation'      => 'required|mimes:jpeg,png'
        ], $messages);

        if ($validator->fails()) {
            return response()->json(array('exito' => false), 200);
        } else {
            $document = $request->all();

            $file = $request->file('documentation');
            $documentation = "";

            if ($file != null) {
                $size = $file->getClientSize();
                $extension = $file->getClientOriginalExtension();
                $documentation = md5(time()) . "." . $extension;
                $maximo = 2;

                if ($size > 0) {
                    if (($size / 1000000) > $maximo){
                        return response()->json(array('exito' => false), 200);
                    }
                } else {
                    return response()->json(array('exito' => false), 200);
                }

                \Storage::disk('image')->put('categories/' . $documentation,  \File::get($file));

                return response()->json(array('exito' => true, 'file' => $documentation), 200);
            }
        }

        return response()->json(array('exito' => false), 200);
    }
}
