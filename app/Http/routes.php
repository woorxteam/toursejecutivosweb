<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'PageController@index');
Route::get('/acerca-de-nosotros', 'PageController@acerca');
Route::get('/contacto', 'PageController@contacto');
Route::post('/contacto', 'PageController@send');

Route::get('/tiida', 'PageController@tiida');
Route::get('/galeria', 'PageController@galeria');
Route::get('/galeria/{id}', 'PageController@detallelGaleria');
Route::get('/blog/{id}', 'PageController@show');
Route::get('/blog', 'PageController@blog');

Route::get('/volksbus', 'PageController@volksbus');
Route::get('/camion', 'PageController@camion');
Route::get('/escalade', 'PageController@escalade');
Route::get('/crafter', 'PageController@crafter');
Route::get('/passat', 'PageController@passat');
Route::get('/suburban', 'PageController@suburban');
Route::get('/urvan', 'PageController@urvan');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);

Route::group(['middleware'  => 'auth'], function(){
    Route::resource('/publications', 'PublicationController');

    Route::delete('/gallery/{id}/photo/{photo}/delete','GalleryController@destroyPhoto');
    Route::get('/gallery/{id}/detail','GalleryController@show');
    Route::post('/gallery/{id}/photos','GalleryController@storePhotos');
    Route::get('/gallery/{id}/photos','GalleryController@createPhotos');
    Route::resource('/gallery', 'GalleryController');
    Route::resource('/categories', 'CategoryController');

    Route::post('/image/upload/publication','PublicationController@upload');
    Route::put('/image/upload/publication','PublicationController@upload');
    Route::post('/image/upload','GalleryController@upload');
    Route::post('/image/upload/category','CategoryController@upload');
    Route::put('/image/upload/category','CategoryController@upload');

    Route::post('/image/upload/album','GalleryController@upload');
    Route::put('/image/upload/album','GalleryController@upload');
});

Route::pattern('inexistentes', '.*');
Route::any('/{inexistentes}', function()
{
	 return view('error');
});
