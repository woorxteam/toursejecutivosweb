<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'id', 'status'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public static function categories()
    {
        $categories = \DB::table('categories')
                        ->select('id', 'name', 'status', 'type',
                                \DB::raw("IF(status = 'active','Activo','Inactivo') AS estado"),
                                \DB::raw("case type
                                   when 'publication' then 'Publicación'
                                   when 'gallery' then 'Galeria'
                                   when 'both' then 'ambos'
                                end as tipo")
                        )
                        ->where('status', '<>', 'deleted')
                        ->get();

        return $categories;
    }

    public static function list_categories($type)
    {
        $categories = \DB::table('categories')
                        ->select('id', 'name', 'status', 'image',
                                \DB::raw("IF(status = 'active','Activo','Inactivo') AS estado")
                        )
                        ->where('status', 'active')
                        ->where(function ($query) use ($type) {
                          $query->where('type', $type)
                                ->orWhere('type', 'both');
                        })
                        ->get();

        return $categories;
    }

}
