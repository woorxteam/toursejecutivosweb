<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'publications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'subtitle', 'description', 'type', 'status'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public static function publications()
    {
        $publications = \DB::table('publications')
                        ->select('title', 'subtitle', 'id', 'description', 'type', 'image',
                          \DB::raw("IF(status = 'active','Activo','Inactivo') AS estado"),
                          \DB::raw("IF(type = 'content','Publicación de contenido','Video') AS tipo_publicacion"),
                          \DB::raw("DATE_FORMAT(created_at, '%d/%m/%Y') AS fecha"),
                          \DB::raw("DATE_FORMAT(created_at, '%m_%Y') AS creacion"),
                          \DB::raw("CASE status
                                      WHEN 'active' THEN 'Publicada'
                                      WHEN 'inactive' THEN 'Borrador'
                          END AS estado")
                        )
                        ->where('status', '<>', 'deleted')
                        ->get();

        return $publications;
    }


    public static function publications_blog()
    {
        $publications = \DB::table('publications')
                        ->select('title', 'subtitle', 'id', 'description', 'type', 'image', 'slug',
                          \DB::raw("IF(status = 'active','Activo','Inactivo') AS estado"),
                          \DB::raw("IF(type = 'content','Publicación de contenido','Video') AS tipo_publicacion"),
                          \DB::raw("DATE_FORMAT(created_at, '%d/%m/%Y') AS fecha"),
                          \DB::raw("DATE_FORMAT(created_at, '%d') AS dia"),
                          \DB::raw("DATE_FORMAT(created_at, '%Y') AS anio"),
                          \DB::raw("DATE_FORMAT(created_at, '%m_%Y') AS creacion"),
                          \DB::raw("CASE DATE_FORMAT(created_at, '%m')
                                            WHEN '01' THEN 'Enero'
                                            WHEN '02' THEN 'Febrero'
                                            WHEN '03' THEN 'Marzo'
                                            WHEN '04' THEN 'Abril'
                                            WHEN '05' THEN 'Mayo'
                                            WHEN '06' THEN 'Junio'
                                            WHEN '07' THEN 'Julio'
                                            WHEN '08' THEN 'Agosto'
                                            WHEN '09' THEN 'Septiembre'
                                            WHEN '10' THEN 'Octubre'
                                            WHEN '11' THEN 'Noviembre'
                                            WHEN '12' THEN 'Diciembre'
                                         END AS mes")
                        )
                        ->where('status', '<>', 'deleted');

        $publications = $publications->paginate(9);

        return $publications;
    }


    public static function publication_detail($slug)
    {
        $publication = \DB::table('publications')
                        ->select('publications.title', 'publications.subtitle', 'publications.id', 'publications.description', 'publications.type',
                                 'publications.image', 'publications.slug', 'categories.name AS category', 'users.name AS autor',
                                  \DB::raw("IF(publications.status = 'active','Activo','Inactivo') AS estado"),
                                  \DB::raw("IF(publications.type = 'content','Publicación de contenido','Video') AS tipo_publicacion"),
                                  \DB::raw("DATE_FORMAT(publications.created_at, '%d/%m/%Y') AS fecha"),
                                  \DB::raw("DATE_FORMAT(publications.created_at, '%d') AS dia"),
                                  \DB::raw("DATE_FORMAT(publications.created_at, '%m_%Y') AS creacion"),
                                  \DB::raw("CASE DATE_FORMAT(publications.created_at, '%m')
                                            WHEN '01' THEN 'Enero'
                                            WHEN '02' THEN 'Febrero'
                                            WHEN '03' THEN 'Marzo'
                                            WHEN '04' THEN 'Abril'
                                            WHEN '05' THEN 'Mayo'
                                            WHEN '06' THEN 'Junio'
                                            WHEN '07' THEN 'Julio'
                                            WHEN '08' THEN 'Agosto'
                                            WHEN '09' THEN 'Septiembre'
                                            WHEN '10' THEN 'Octubre'
                                            WHEN '11' THEN 'Noviembre'
                                            WHEN '12' THEN 'Diciembre'
                                         END AS mes")
                        )
                        ->join('categories', 'publications.category_id', '=', 'categories.id')
                        ->join('users', 'publications.created_by', '=', 'users.id')
                        ->where('publications.slug', $slug)
                        ->where('publications.status', '<>', 'deleted')
                        ->first();

        return $publication;
    }

}
