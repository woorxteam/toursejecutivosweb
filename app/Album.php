<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'album';

    public static function albums()
    {
        $albums = \DB::table('album')
                  ->select('title', 'image', 'status', 'id',
                            \DB::raw("DATE_FORMAT(created_at, '%m_%Y') AS creacion"),
                            \DB::raw("CASE status
                                        WHEN 'active' THEN 'Publicada'
                                        WHEN 'inactive' THEN 'Borrador'
                                      END AS estado")
                            )
                  ->where('status', '<>', 'deleted')
                  ->get();

        return $albums;
    }



    public static function albumsPage()
    {
        $albums = \DB::table('album')
                  ->select('title', 'image', 'status', 'id',
                            \DB::raw("DATE_FORMAT(created_at, '%m_%Y') AS creacion"),
                            \DB::raw("CASE status
                                        WHEN 'active' THEN 'Publicada'
                                        WHEN 'inactive' THEN 'Borrador'
                                      END AS estado")
                            )
                  ->where('status', 'active')
                  ->get();

        return $albums;
    }


    public static function detail($id)
    {
        $albums = \DB::table('album')
                  ->select('title', 'image', 'status', 'id',
                            \DB::raw("DATE_FORMAT(created_at, '%m_%Y') AS creacion"),
                            \DB::raw("CASE status
                                        WHEN 'active' THEN 'Publicada'
                                        WHEN 'inactive' THEN 'Borrador'
                                      END AS estado")
                            )
                  ->where('id', $id)
                  ->first();

        return $albums;
    }
}
