<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
abstract class Job
class ChangeLocale extends Job implements SelfHandling
{
    protected $lang;

    public function __construct($lang)
    {
        $this->lang = $lang;
    }

    public function handle()
    {
        session()->put('locale',$this->lang);
    }
}
