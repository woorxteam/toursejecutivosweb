<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'photos_album';

    public static function galleries($id)
    {
        $galleries = \DB::table('photos_album')
                      ->select('photos_album.photo', 'photos_album.id',
                               \DB::raw("DATE_FORMAT(album.created_at, '%m_%Y') AS creacion")
                      )
                      ->join('album', 'photos_album.album_id', '=', 'album.id')
                      ->where('album.id', $id)
                      ->get();

        return $galleries;
    }


    public static function galleryDetail($id)
    {
        $gallery = \DB::table('photos_album')
                    ->select('photos_album.photo', 'photos_album.id',
                             \DB::raw("DATE_FORMAT(album.created_at, '%m_%Y') AS creacion")
                    )
                    ->join('album', 'photos_album.album_id', '=', 'album.id')
                    ->where('photos_album.id', $id)
                    ->first();

        return $gallery;
    }
}
