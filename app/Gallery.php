<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gallery';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public static function galleries()
    {
        $galleries = \DB::table('gallery')
                        ->select('gallery.id', 'gallery.title', 'gallery.file', 'categories.name AS category')
                        ->join('categories', 'gallery.category_id', '=', 'categories.id')
                        ->get();

        return $galleries;
    }


    public static function galleries_category($category)
    {
        $galleries = \DB::table('gallery')
                        ->select('gallery.id', 'gallery.title', 'gallery.file', 'categories.name AS category')
                        ->join('categories', 'gallery.category_id', '=', 'categories.id')
                        ->where('category_id', $category)
                        ->get();

        return $galleries;
    }

}
