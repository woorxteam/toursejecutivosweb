<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'id'              => 1,
            'name'            => 'Alejandro García Santacruz',
            'email'           => 'alex@woorx.mx',
            'password'        => bcrypt('Al3jandr0'),
            'remember_token'  => str_random(10),
        ]);
    }
}
