<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosAlbumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos_album', function (Blueprint $table) {
            $table->increments('id');
            $table->string('photo');
            $table->integer('album_id')->unsigned();
            $table->foreign('album_id')
                ->references('id')
                ->on('album')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('photos_album');
    }
}
