<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('publications', function (Blueprint $table) {
          $table->increments('id');
          $table->string('title');
          $table->string('subtitle');
          $table->string('slug')->unique();
          $table->string('image')->nullable();
          $table->longText('description');
          $table->enum('status', ['active', 'inactive', 'deleted']);
          $table->enum('type', ['content', 'video']);
          $table->integer('category_id')->unsigned();
          $table->foreign('category_id')
              ->references('id')
              ->on('categories')
              ->onDelete('cascade');
          $table->integer('created_by')->unsigned();
          $table->foreign('created_by')
              ->references('id')
              ->on('users')
              ->onDelete('cascade');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('publications');
    }
}
