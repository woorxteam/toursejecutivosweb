<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Tours Ejecutivos - Transporte de personal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="//fast.eager.io/1fe0H7y44S.js"></script>
    <link href="/page/css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/page/css/themify-icons.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/page/css/flexslider.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/page/css/lightbox.min.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/page/css/ytplayer.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/page/css/styles.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/page/css/custom.css" rel="stylesheet" type="text/css" media="all"/>
    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400%7CRaleway:100,400,300,500,600,700%7COpen+Sans:400,500,600' rel='stylesheet' type='text/css'>
    @yield('styles')
</head>
<!--



8b      db      d8   ,adPPYba,    ,adPPYba,   8b,dPPYba,  8b,     ,d8
`8b    d88b    d8'  a8"     "8a  a8"     "8a  88P'   "Y8   `Y8, ,8P'
 `8b  d8'`8b  d8'   8b       d8  8b       d8  88             )888(
  `8bd8'  `8bd8'    "8a,   ,a8"  "8a,   ,a8"  88           ,d8" "8b,
    YP      YP       `"YbbdP"'    `"YbbdP"'   88          8P'     `Y8

    Sitio hecho por: Woorx
    Copyright: WoorxStuff S.A de C.V
    WWW.WOORX.MX

-->
<body class="scroll-assist">
<div id="main-container" class="main-container">
    <div class="nav-container">
        <a id="top"></a>
        <nav class="bg-white">
            <div class="nav-bar">
                <div class="module left">
                    <a href="/">
                        <img class="logo logo-light" alt="Tours Ejecutivos" src="/page/img/logo-tours.png"/>
                        <img class="logo logo-dark" alt="Tours Ejecutivos" src="/page/img/logo-tours.png"/>
                    </a>
                </div>
                <div class="module widget-handle mobile-toggle right visible-sm visible-xs">
                    <i class="ti-menu"></i>
                </div>
                <div class="module-group right">
                    <div class="module left">
                        <ul class="menu">
                            <li>
                                <a href="/">
                                    {{ trans('master.inicio') }}
                                </a>
                            </li>
                            <li>
                                <a href="/acerca-de-nosotros">
                                    {{ trans('master.nosotros') }}
                                </a>
                            </li>
                            <li>
                                <a class="toggleNav">
                                    {{ trans('master.modelos') }}
                                </a>
                            </li>
                            <li>
                                <a href="/galeria">
                                    {{ trans('master.galeria') }}
                                </a>
                            </li>
                            <li>
                                <a href="/blog">
                                    {{ trans('master.blog') }}
                                </a>
                            </li>
                            <li>
                                <a href="/contacto">
                                    {{ trans('master.contacto') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="module widget-handle language left">
                      <ul class="menu">
                          <li class="has-dropdown">
                              <a>{{ trans('master.lang') }}</a>
                              <ul>
                                  <li>
                                      <a href='#' class="lang" id="en">English</a>
                                  </li>
                                  <li>
                                      <a href='#' class="lang" id="es">Español</a>
                                  </li>
                              </ul>
                          </li>
                      </ul>
                  </div>
                </div>
                <!--terminan modulos de menú-->
            </div>
        </nav>
    </div>
    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn toggleNav">&times;</a>
        <div class="list">
            <div class="elem">
                <a href="/tiida">
                    <img style="width:70%;margin:0 auto;display:block" src="/page/img/perfiles/auto-1-perfil.png"/>
                    <h4 class="text-center">Tiida</h4>
                </a>
            </div>
            <div class="elem">
                <a href="/passat">
                    <img style="width:70%;margin:0 auto;display:block" src="/page/img/perfiles/auto-2-perfil.png"/>
                    <h4 class="text-center">Passat</h4>
                </a>
            </div>
            <div class="elem">
                <a href="/suburban">
                    <img style="width:70%;margin:0 auto;display:block" src="/page/img/perfiles/suburban-perfil.png"/>
                    <h4 class="text-center">Suburban</h4>
                </a>
            </div>
            <div class="elem">
                <a href="/escalade">
                    <img style="width:70%;margin:0 auto;display:block" src="/page/img/perfiles/escalade-perfil.png"/>
                    <h4 class="text-center">Escalade</h4>
                </a>
            </div>
            <div class="elem">
                <a href="/urvan">
                    <img style="width:70%;margin:0 auto;display:block" src="/page/img/perfiles/van-1-perfil.png"/>
                    <h4 class="text-center">Urvan</h4>
                </a>
            </div>
            <div class="elem">
                <a href="/crafter">
                    <img style="width:70%;margin:0 auto;display:block" src="/page/img/perfiles/van-2-perfil.png"/>
                    <h4 class="text-center">Crafter VW</h4>
                </a>
            </div>
            <div class="elem">
                <a href="/volksbus">
                    <img style="width:70%;margin:0 auto;display:block" src="/page/img/perfiles/camion-1-perfil.png"/>
                    <h4 class="text-center">Man Bus & Truck VW</h4>
                </a>
            </div>
            <div class="elem">
                <a href="/camion">
                    <img style="width:70%;margin:0 auto;display:block" src="/page/img/perfiles/camion-2-perfil.png"/>
                    <h4 class="text-center">International</h4>
                </a>
            </div>
        </div>
        <div class="up text-center"><span class="ti-angle-up"></span></div>
        <div class="down text-center"><span class="ti-angle-down"></span></div>
    </div>
    @yield('content')
    <footer class="footer-1 bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-10">
                    <img alt="Tours Ejecutivos" class="logo" src="/page/img/logo-light.png"/>
                </div>
                <div class="col-md-4 col-md-offset-3 col-sm-12">
                    <div class="widget">
                        <h6 class="title">{{ trans('master.mapa') }}</h6>
                        <hr>
                        <ul class="">
                            <li>
                                <a class="toggleNav">{{ trans('master.modelos') }}</a>
                            </li>
                            <li>
                                <a href="/galeria">{{ trans('master.galeria') }}</a>
                            </li>
                            <li>
                                <a href="/blog">{{ trans('master.blog') }}</a>
                            </li>
                            <li>
                                <a href="/contacto">{{ trans('master.contacto') }}</a>
                            </li>
                        </ul>
                    </div>
                    <!--end of widget-->
                </div>
                <img src="" alt="">
                <!--<div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h6 class="title">Latest Updates</h6>
                        <hr>
                        <div class="twitter-feed">
                            <div class="tweets-feed" data-widget-id="492085717044981760">
                            </div>
                        </div>
                    </div>
                    &lt;!&ndash;end of widget&ndash;&gt;
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h6 class="title">Instagram</h6>
                        <hr>
                        <div class="instafeed" data-user-name="funsizeco">
                            <ul></ul>
                        </div>
                    </div>
                    &lt;!&ndash;end of widget&ndash;&gt;
                </div>-->
            </div>
            <!--widgets de footer-->
            <div class="row">
                <div class="col-sm-6">
                    <span class="sub" style="font-size:10px;">&copy; Copyright 2016 / 2017 - Tours Ejecutivos - Diseño y Desarrollo por: <a href="http://woorx.mx" target="_blank">Woorx #WeMakeCoolStuff</a></span>
                </div>
                <div class="col-sm-6 text-right">
                    <ul class="list-inline social-list">
                        <li>
                            <a href="https://twitter.com/ToursEje" target="_blank">
                                <i class="ti-twitter-alt"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/ToursEje" target="_blank">
                                <i class="ti-facebook"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--termina contenedor-->
        <a class="btn btn-sm fade-half back-to-top inner-link" href="#top">Top</a>
    </footer>
</div>

<script src="https://code.jquery.com/jquery-2.2.4.js"></script>
<script src="/page/js/bootstrap.min.js"></script>
<script src="/page/js/flickr.js"></script>
<script src="/page/js/flexslider.min.js"></script>
<script src="/page/js/lightbox.min.js"></script>
<script src="/page/js/masonry.min.js"></script>
<script src="/page/js/twitterfetcher.min.js"></script>
<script src="/page/js/spectragram.min.js"></script>
<script src="/page/js/ytplayer.min.js"></script>
<script src="/page/js/countdown.min.js"></script>
<script src="/page/js/smooth-scroll.min.js"></script>
<script src="/page/js/parallax.js"></script>
<script src="/page/js/scripts.js"></script>
<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
<script type="text/javascript">
$('.main-carousel').flickity({
  // options
  cellAlign: 'left',
  contain: true
  });
</script>
@yield('scripts')
</body>
</html>
