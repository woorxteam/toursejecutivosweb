@extends('layout')
@section('content')
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Categorías - Editar categoría</h3>
        </div>

        <div class="title_right">
          <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group"><p>&nbsp;</p></div>
          </div>
        </div>
      </div>

      <div class="clearfix"></div>

      <div class="row">
        @if (count($errors) > 0)
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                Por favor corrige los siguientes errores:<br>
                @foreach ($errors->all() as $error)
                  {{ $error }}<br />
                @endforeach
            </div>
        @endif

        <div class="col-md-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Información general <small>Completa todos los campos del formulario</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              {!! Form::model($category, array('route' => array('categories.update', $category->id), 'id' => 'form', 'method' => 'PUT', 'class'=>'form-horizontal')) !!}

                <div class="form-group col-md-6">
                  <label class="control-label col-md-2 col-sm-3 col-xs-12">Nombre</label>
                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" class="form-control has-feedback-left" name="name" placeholder="Nombre" value="{{ $category->name }}">
                  </div>
                </div>

                <div class="form-group col-md-6">
                  <label class="control-label col-md-2 col-sm-3 col-xs-12">Status</label>
                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <select class="form-control" name="status">
                      <option value="active" @if ($category->status == 'active')selected="selected"@endif>Activa</option>
                      <option value="inactive" @if ($category->status == 'inactive')selected="selected"@endif>Inactiva</option>
                    </select>
                  </div>
                </div>

                <div class="form-group col-md-6">
                  <label class="control-label col-md-2 col-sm-3 col-xs-12">Tipo</label>
                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <select class="form-control" name="type">
                      <option value="publication" @if ($category->type == 'publication')selected="selected"@endif>Publicación</option>
                      <option value="gallery" @if ($category->type == 'gallery')selected="selected"@endif>Galeria</option>
                      <option value="both" @if ($category->type == 'both')selected="selected"@endif>Ambos</option>
                    </select>
                  </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Imagen</label>
                    <div class="col-md-10">
                        <div class="input-group" id="document_campo">
                            <span class="input-group-addon"><span class="fa fa-image"></span></span>
                            <input type="file" name="document" id="document" class="form-control" onChange="upload_image_category('document', '')" />
                            <input type="hidden" name="document_name" id="document_name" value="" />
                        </div>
                        <span class="help-block" id="document_descripcion">
                          Solo se permiten archivos tipo JPG y PNG  con un tamaño máximo de 2mb.
                        </span>
                        <span id="upload_document" class="help-block" style="display:none; color:#1caf9a !important">Validando el archivo... <i class="fa fa-refresh fa-spin fa-2x"></i></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-9 col-sm-9 col-xs-12">&nbsp;</div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                    <button type="reset" class="btn btn-primary">Cancelar</button>
                    <button type="submit" class="btn btn-success copyeditor">Guardar</button>
                  </div>
                </div>
              {!! Form::close() !!}
            </div>
          </div>
      </div>
    </div>
    </div>
@endsection

@section('scripts')
      @parent

      <script type="text/javascript">
          var token_ = "{{ csrf_token() }}";
      </script>
      <script type="text/javascript" src="{{ asset('js/scripts.js') }}"></script>
@endsection
