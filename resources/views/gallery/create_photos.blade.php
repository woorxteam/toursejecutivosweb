@extends('layout')
@section('content')
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Galería - Nueva galería</h3>
        </div>

        <div class="title_right">
          <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group"><p>&nbsp;</p></div>
          </div>
        </div>
      </div>

      <div class="clearfix"></div>

      <div class="row">
        @if (count($errors) > 0)
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                Por favor corrige los siguientes errores:<br>
                @foreach ($errors->all() as $error)
                  {{ $error }}<br />
                @endforeach
            </div>
        @endif
      </div>

      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Fotos del album</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <p>Arrastre varios archivos al cuadro de abajo para cargar imagenes múltiples o haga clic para seleccionar archivos.<br />
                 Nota: Solo se permiten imagenes tipo JPG y PNG
              </p>
              <form action="/gallery/{{ $album->id }}/photos" class="dropzone">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
              </form>
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('scripts')
      @parent

      <script type="text/javascript">
          var token_ = "{{ csrf_token() }}";
      </script>
      <script type="text/javascript" src="{{ asset('js/scripts.js') }}"></script>

      <!-- FastClick -->
      <script src="{{ asset('gentelella/fastclick/lib/fastclick.js') }}"></script>
      <!-- NProgress -->
      <script src="{{ asset('gentelella/nprogress/nprogress.js') }}"></script>
      <!-- Dropzone.js -->
      <script src="{{ asset('gentelella/dropzone/dist/min/dropzone.min.js') }}"></script>


@endsection
