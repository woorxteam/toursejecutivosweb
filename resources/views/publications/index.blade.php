@extends('layout')
@section('content')
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Publicaciones</h3>
        </div>

        <div class="title_right">
          <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
              <a href="/publications/create">
                  <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-plus"></i> Nueva publicación</button>
              </a>
            </div>
          </div>
        </div>
      </div>

      <div class="clearfix"></div>

      <div class="row">


        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2><small>Listado de publicaciones</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">

              <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th width="35%">Título</th>
                    <th>Subtítulo</th>
                    <th width="10%">Fecha de creación</th>
                    <th width="10%">Status</th>
                    <th width="15%">Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  @if (count($publications) > 0)
                      @foreach($publications AS $publication)
                          <tr>
                            <th>{{$publication->title}}</th>
                            <th>{{$publication->subtitle}}</th>
                            <th>{{$publication->fecha}}</th>
                            <th>{{$publication->estado}}</th>
                            <th>
                              <table width="100%">
                                  <tr>
                                      <td width="50%" align="center">
                                        <a href="publications/{{ $publication->id }}/edit">
                                          <button type="button" class="btn btn-warning"><i class="fa fa-pencil"></i></button>
                                        </a>
                                      </td>

                                      <td width="50%" align="center">
                                          <button type="button" class="btn btn-danger deleteBtn" rel="{{ $publication->id }}" data-toggle="modal" data-target="#myModal"><i class="fa fa-trash"></i></button>
                                      </td>
                                  </tr>
                              </table>
                            </th>
                          </tr>
                      @endforeach
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>


    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><span class="fa fa-times"></span> Advertencia</h4>
            </div>
            <div class="modal-body">
              <p>¿Está seguro que desea eliminar esta publicación?</p>
            </div>
            <div class="modal-footer">
              <div class="col-md-12 pull-left">
                    {!! Form::open(array('url' => 'publications', 'id'  => 'deleteForm')) !!}
                      {!! Form::hidden('_method', 'DELETE') !!}
                      {!! Form::submit('Eliminar', array('class' => 'btn btn-danger btn-lg pull-left')) !!}
                      <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Cancelar</button>
                    {!! Form::close() !!}

              </div>
            </div>
          </div>

        </div>
    </div>
@endsection

@section('scripts')
      @parent
      <!-- Datatables -->
      <script>
        $(document).ready(function() {
          $( ".deleteBtn" ).click(function() {
              var id = $(this).attr("rel");
              $('#deleteForm').attr('action', 'publications/' + id);
          });



          var handleDataTableButtons = function() {
            if ($("#datatable-buttons").length) {
              $("#datatable-buttons").DataTable({
                dom: "Bfrtip",
                buttons: [
                  {
                    extend: "copy",
                    className: "btn-sm"
                  },
                  {
                    extend: "csv",
                    className: "btn-sm"
                  },
                  {
                    extend: "excel",
                    className: "btn-sm"
                  },
                  {
                    extend: "pdfHtml5",
                    className: "btn-sm"
                  },
                  {
                    extend: "print",
                    className: "btn-sm"
                  },
                ],
                responsive: true
              });
            }
          };

          TableManageButtons = function() {
            "use strict";
            return {
              init: function() {
                handleDataTableButtons();
              }
            };
          }();

          $('#datatable').dataTable();

          $('#datatable-keytable').DataTable({
            keys: true
          });

          $('#datatable-responsive').DataTable();

          $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
          });

          $('#datatable-fixed-header').DataTable({
            fixedHeader: true
          });

          var $datatable = $('#datatable-checkbox');

          $datatable.dataTable({
            'order': [[ 1, 'asc' ]],
            'columnDefs': [
              { orderable: false, targets: [0] }
            ]
          });
          $datatable.on('draw.dt', function() {
            $('input').iCheck({
              checkboxClass: 'icheckbox_flat-green'
            });
          });

          TableManageButtons.init();
        });
      </script>
      <!-- /Datatables -->
@endsection
