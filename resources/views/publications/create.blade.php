@extends('layout')
@section('content')
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Publicaciones - Nueva publicación</h3>
        </div>

        <div class="title_right">
          <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group"><p>&nbsp;</p></div>
          </div>
        </div>
      </div>

      <div class="clearfix"></div>

      <div class="row">
        @if (count($errors) > 0)
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                Por favor corrige los siguientes errores:<br>
                @foreach ($errors->all() as $error)
                  {{ $error }}<br />
                @endforeach
            </div>
        @endif

        <div class="col-md-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Información general <small>Completa todos los campos del formulario</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form class="form-horizontal form-label-left input_mask" method="POST" action="/publications" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" class="form-control has-feedback-left" name="title" placeholder="Título">
                  <span class="fa fa-pencil form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" class="form-control" name="subtitle" placeholder="Subtítulo">
                  <span class="fa fa-pencil form-control-feedback right" aria-hidden="true"></span>
                </div>


                <div class="form-group col-md-6">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Tipo de publicación</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <select class="form-control" name="type">
                      <option value="content">Publicación de contenido</option>
                      <option value="video">Video</option>
                    </select>
                  </div>
                </div>

                <div class="form-group col-md-6">
                  <label class="control-label col-md-2 col-sm-3 col-xs-12">Status</label>
                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <select class="form-control" name="status">
                      <option value="active">Publicada</option>
                      <option value="inactive">Borrador</option>
                    </select>
                  </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <div class="col-md-10">
                        <div class="input-group" id="document_campo">
                            <span class="input-group-addon"><span class="fa fa-image"></span></span>
                            <input type="file" name="document" id="document" class="form-control" onChange="upload_image_publication('document', '')" />
                            <input type="hidden" name="document_name" id="document_name" value="" />
                        </div>
                        <span class="help-block" id="document_descripcion">
                          Solo se permiten archivos tipo JPG y PNG  con un tamaño máximo de 2mb.
                        </span>
                        <span id="upload_document" class="help-block" style="display:none; color:#1caf9a !important">Validando el archivo... <i class="fa fa-refresh fa-spin fa-2x"></i></span>
                    </div>
                </div>

                <div class="form-group col-md-6">
                  <label class="control-label col-md-2 col-sm-3 col-xs-12">Categoría</label>
                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <select class="form-control" name="category" required="required">
                      <option value="">Selecciona la categoría</option>
                      @if (count($categories) > 0)
                          @foreach($categories AS $category)
                              <option value="{{ $category->id }}">{{ $category->name }}</option>
                          @endforeach
                      @endif
                    </select>
                  </div>
                </div>

                <div class="form-group col-md-12">
                  <div class="x_content">
                    <div id="alerts"></div>
                    <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor">
                      <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                        </ul>
                      </div>

                      <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                          <li>
                            <a data-edit="fontSize 5">
                              <p style="font-size:17px">Huge</p>
                            </a>
                          </li>
                          <li>
                            <a data-edit="fontSize 3">
                              <p style="font-size:14px">Normal</p>
                            </a>
                          </li>
                          <li>
                            <a data-edit="fontSize 1">
                              <p style="font-size:11px">Small</p>
                            </a>
                          </li>
                        </ul>
                      </div>

                      <div class="btn-group">
                        <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                        <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                        <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                        <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                      </div>

                      <div class="btn-group">
                        <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                        <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                        <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
                        <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                      </div>

                      <div class="btn-group">
                        <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                        <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                        <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                        <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                      </div>

                      <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                        <div class="dropdown-menu input-append">
                          <input class="span2" placeholder="URL" type="text" data-edit="createLink" />
                          <button class="btn" type="button">Add</button>
                        </div>
                        <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
                      </div>

                      <div class="btn-group">
                        <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="fa fa-picture-o"></i></a>
                        <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
                      </div>

                      <div class="btn-group">
                        <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                        <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                      </div>
                    </div>

                    <div id="editor" class="editor-wrapper" data-target="content"></div>
                    <textarea type="text" class="hidden" name="description" id="content" style="display:none;"></textarea>
                    <br />
                    <div class="ln_solid"></div>
                  </div>
                </div>

                <div class="form-group">
                    <div class="col-md-9 col-sm-9 col-xs-12">&nbsp;</div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                    <button type="reset" class="btn btn-primary">Cancelar</button>
                    <button type="submit" class="btn btn-success copyeditor">Guardar</button>
                  </div>
                </div>

              </form>
            </div>
          </div>

      </div>
    </div>
    </div>
@endsection

@section('scripts')
      @parent

      <!-- bootstrap-wysiwyg -->
      <script src="{{ asset('gentelella/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"></script>
      <script src="{{ asset('gentelella/jquery.hotkeys/jquery.hotkeys.js') }}"></script>
      <script src="{{ asset('gentelella/google-code-prettify/src/prettify.js') }}"></script>

      <script>
        $(document).ready(function() {

          $(".copyeditor").on("click", function() {
             var targetName = $("#editor").attr('data-target');
             $('#'+targetName).val($('#editor').html());
          });

          function initToolbarBootstrapBindings() {
            var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                'Times New Roman', 'Verdana'
              ],
              fontTarget = $('[title=Font]').siblings('.dropdown-menu');
            $.each(fonts, function(idx, fontName) {
              fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
            });
            $('a[title]').tooltip({
              container: 'body'
            });
            $('.dropdown-menu input').click(function() {
                return false;
              })
              .change(function() {
                $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
              })
              .keydown('esc', function() {
                this.value = '';
                $(this).change();
              });

            $('[data-role=magic-overlay]').each(function() {
              var overlay = $(this),
                target = $(overlay.data('target'));
              overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
            });

            if ("onwebkitspeechchange" in document.createElement("input")) {
              var editorOffset = $('#editor').offset();

              $('.voiceBtn').css('position', 'absolute').offset({
                top: editorOffset.top,
                left: editorOffset.left + $('#editor').innerWidth() - 35
              });
            } else {
              $('.voiceBtn').hide();
            }
          }

          function showErrorAlert(reason, detail) {
            var msg = '';
            if (reason === 'unsupported-file-type') {
              msg = "El formato no es válido, solo se pueden subir imagenes";
            } else {
              console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
              '<strong>Error al subir la imagen</strong> ' + msg + ' </div>').prependTo('#alerts');
          }

          initToolbarBootstrapBindings();

          $('#editor').wysiwyg({
            fileUploadError: showErrorAlert
          });

          window.prettyPrint;
          prettyPrint();
        });
      </script>
      <!-- /bootstrap-wysiwyg -->

      <script type="text/javascript">
          var token_ = "{{ csrf_token() }}";
      </script>
      <script type="text/javascript" src="{{ asset('js/scripts.js') }}"></script>


      <script>
        $(document).ready(function() {

          $(".copyeditor").on("click", function() {
             var targetName = $("#editor").attr('data-target');
             $('#'+targetName).val($('#editor').html());
          });

          function initToolbarBootstrapBindings() {
            var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                'Times New Roman', 'Verdana'
              ],
              fontTarget = $('[title=Font]').siblings('.dropdown-menu');
            $.each(fonts, function(idx, fontName) {
              fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
            });
            $('a[title]').tooltip({
              container: 'body'
            });
            $('.dropdown-menu input').click(function() {
                return false;
              })
              .change(function() {
                $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
              })
              .keydown('esc', function() {
                this.value = '';
                $(this).change();
              });

            $('[data-role=magic-overlay]').each(function() {
              var overlay = $(this),
                target = $(overlay.data('target'));
              overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
            });

            if ("onwebkitspeechchange" in document.createElement("input")) {
              var editorOffset = $('#editor').offset();

              $('.voiceBtn').css('position', 'absolute').offset({
                top: editorOffset.top,
                left: editorOffset.left + $('#editor').innerWidth() - 35
              });
            } else {
              $('.voiceBtn').hide();
            }
          }

          function showErrorAlert(reason, detail) {
            var msg = '';
            if (reason === 'unsupported-file-type') {
              msg = "El formato no es válido, solo se pueden subir imagenes";
            } else {
              console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
              '<strong>Error al subir la imagen</strong> ' + msg + ' </div>').prependTo('#alerts');
          }

          initToolbarBootstrapBindings();

          $('#editor').wysiwyg({
            fileUploadError: showErrorAlert
          });

          window.prettyPrint;
          prettyPrint();
        });
      </script>
      <!-- /bootstrap-wysiwyg -->

@endsection
