@extends('../layouts/master')

@section('content')
    <section class="page-title page-title-1 image-bg overlay parallax">
        <div class="background-image-holder ajuste">
            <img alt="Background Image" class="background-image" src="page/img/header-nosotros-1.jpg"/>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2 class="uppercase mb0">{{ trans('acerca.titulo-nosotros') }}</h2>
                    <h4 class="uppsercase mb0">{{ trans('acerca.descripcion-nosotros') }}</h4>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
        <ol class="breadcrumb breadcrumb-2">
            <li>
                <a href="/">Inicio</a>
            </li>
            <li style="font-weight:400">Quiénes somos</li>
        </ol>;
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h2 style="color:#0B24FB;" class="number">{{ trans('acerca.quienes-somos') }}</h2>
                    <p>
                      {!! trans('acerca.acerca-de-nosotros') !!}
                    </p>
                </div>
                <div class="col-sm-6">
                    <img src="page/img/home-2-7.jpg"/>
                </div>

            </div>
            <!--end of row-->
        </div>
        <hr class="mb0">
        <div class="container pt40">
            <div class="row">
                <div class="col-sm-4">
                    <h2 class="number" style="color:#0B24FB">{{ trans('acerca.mision-titulo') }}</h2>
                    <p>
                        {{ trans('acerca.mision') }} </p>
                </div>
                <div class="col-sm-4">
                    <h2 class="number" style="color:#0B24FB">{{ trans('acerca.vision-titulo') }}</h2>
                    <p>
                        {{ trans('acerca.vision') }}
                    </p>
                </div>
                <div class="col-sm-4">
                    <h2 class="number" style="color:#0B24FB">{{ trans('acerca.pdecalidad-titulo') }}</h2>
                    <p>
                        {{ trans('acerca.pdecalidad') }}
                    </p></br>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 text-center">
                    <div class="feature">
                        <img src="page/img/puntualidad.png">
                        <h4>{{ trans('acerca.puntualidad') }}</h4>

                    </div>
                </div>
                <div class="col-sm-4 text-center">
                    <div class="feature">
                        <img src="page/img/atencion-cliente.png"/>
                        <h4>{{ trans('acerca.atencion') }}</h4>
                        </br>
                    </div>
                </div>
                <div class="col-sm-4 text-center">
                    <div class="feature">
                        <img src="page/img/inovacion.png"/>
                        <h4>{{ trans('acerca.inovación') }}</h4>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of row-->
            <div class="row">
                <div class="col-sm-4 text-center">
                    <div class="feature">
                        <img src="page/img/mejora-continua.png"/>
                        <h4>{{ trans('acerca.mejora') }}</h4>

                    </div>
                </div>
                <div class="col-sm-4 text-center">
                    <div class="feature">
                        <img src="page/img/confianza.png"/>
                        <h4>{{ trans('acerca.confiabilidad') }}</h4>

                    </div>
                </div>
                <div class="col-sm-4 text-center">
                    <div class="feature">
                        <img src="page/img/seguridad.png"/>
                        <h4>{{ trans('acerca.seguridad') }}</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-secondary">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h4 class="uppercase mb16">{{ trans('acerca.Certificaciones') }}</h4>
                <p class="lead mb64"></p>
            </div>
        </div>
        <!--certificaciones-->
  <div class="fluid-container">
    <div class="carousel" data-flickity='{ "imagesLoaded": true, "percentPosition": true, "autoPlay": true, "cellAlign": "left", "contain": true }'>
      <img class"imagencarrusel" src="page/img/certs/cert-01.jpg" alt="Certificado 1" />
      <img class"imagencarrusel" src="page/img/certs/cert-02.jpg" alt="Certificado 2" />
      <img class"imagencarrusel" src="page/img/certs/cert-03.jpg" alt="Certificado 3" />
      <img class"imagencarrusel" src="page/img/certs/cert-04.jpg" alt="Certificado 4" />
      <img class"imagencarrusel" src="page/img/certs/cert-05.jpg" alt="Certificado 5" />
      <img class"imagencarrusel" src="page/img/certs/cert-07.jpg" alt="Certificado 6" />
      <img class"imagencarrusel" src="page/img/certs/cert-08.jpg" alt="Certificado 7" />
      <img class"imagencarrusel" src="page/img/certs/cert-09.jpg" alt="Certificado 8" />
      <img class"imagencarrusel" src="page/img/certs/cert-10.jpg" alt="Certificado 9" />
      <img class"imagencarrusel" src="page/img/certs/cert-11.jpg" alt="Certificado 10" />
      <img class"imagencarrusel" src="page/img/certs/cert-12.jpg" alt="Certificado 12" />

    </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h4 class="uppercase mb16">{{ trans('acerca.nuestrosclientes') }}</h4>
                    <p class="lead mb64">
                        {{ trans('acerca.desc-nuestrosclientes') }}
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="logo-carousel">
                        <ul class="slides">
                            <li>
                                <a href="#">
                                    <img alt="Liverpool" src="page/img/logo-liverpool.svg"/>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img alt="Orsan" src="page/img/logo-orsan.svg"/>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img alt="Oxxo" src="page/img/logo-oxxo.svg"/>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img alt="Siemens" src="page/img/logo-siemens.svg"/>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img alt="Imbera" src="page/img/logo-imbera.svg"/>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img alt="LG" src="page/img/logo-lg.svg"/>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img alt="Samsung" src="page/img/logo-samsung.svg"/>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img alt="Bombardier" src="page/img/logo-bombardier.svg"/>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img alt="Autoliv" src="page/img/logo-autoliv.png"/>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img alt="Unaq" src="page/img/logo-unaq.png"/>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img alt="Harman" src="page/img/logo-harman.png"/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
    </section>
    <hr class="mb0">
    <section class="pt64 pb64" style="background-image:url('page/img/mapa.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2 class="mb8">{{ trans('acerca.instalaciones') }}</h2>
                    <p class="lead mb40">
                        {{ trans('acerca.desc-instalaciones') }}
                    </p>
                    <a href="http://easytrack.mx" target="_blank">
                      <img style="width:120px;margin-bottom: 29px;" src="page/img/logo_easy_track.png" /><br />
                    </a>
                    <a class="btn btn-filled btn-lg mb0" href="/galeria">{{ trans('acerca.conoce') }}</a>
                </div>
            </div>
            <div class="embelish-icons">
                <i class="ti-marker"></i>
                <i class="ti-layout"></i>
                <i class="ti-ruler-alt-2"></i>
                <i class="ti-eye"></i>
                <i class="ti-signal"></i>
                <i class="ti-pulse"></i>
                <i class="ti-marker"></i>
                <i class="ti-layout"></i>
                <i class="ti-ruler-alt-2"></i>
                <i class="ti-eye"></i>
                <i class="ti-signal"></i>
                <i class="ti-pulse"></i>
            </div>
        </div>
    </section>
@endsection
