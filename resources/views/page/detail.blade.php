@extends('../layouts/master')

@section('content')
<section class="page-title page-title-4 bg-secondary">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="uppercase mb0">{{ $publication->title }}</h3>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="post-snippet mb64">
                    @if ($publication->image != "")
                        <img class="mb24" alt="Post Image"
                             src="/images/{{ $publication->creacion }}/{{ $publication->image }}"/>
                    @endif
                    <div class="post-title">
                        <span class="label">{{ $publication->dia }} {{ $publication->mes }}</span>
                        <h4 class="inline-block">{{ $publication->subtitle }}</h4>
                    </div>
                    <ul class="post-meta">
                        <li>
                            <i class="ti-user"></i>
                            <span>Escrito por
                                            <a>{{ $publication->autor }}</a>
                                        </span>
                        </li>
                        <li>
                            <i class="ti-tag"></i>
                            <span>Categoría
                                            <a>{{ $publication->category }}</a>
                                        </span>
                        </li>
                    </ul>
                    <hr>
                    <p>
                        <?php echo html_entity_decode($publication->description); ?>
                    </p>
                </div>
            </div>
            <!--end of nine col-->
        </div>
        <!--end of container row-->
    </div>
    <!--end of container-->
</section>
@endsection
