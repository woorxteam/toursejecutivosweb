@extends('../layouts/master')

@section('content')
    <section style="padding-bottom: 377px !important;padding-top: 270px !important;"
             class="pt240 pb240 parallax image-bg overlay bg-light">
        <div class="background-image-holder background-image-holder2">
            <img alt="image" class="background-image" src="page/img/escalade/escalade.png"/>
        </div>
        <div class="row">
            <div class="col-sm-12 text-right">
                <div style="margin-right:20%">
                    <h1 style="color:#fff;text-shadow: 0 0 30px rgba(0,0,0,.5);">Escalade</h1>
                </div>
            </div>
        </div>
        <!--end of row-->
    </section>
    <section>
        <div class="container pb0">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h4 class="uppercase mb16">{!! trans('escalade.servicios') !!}</h4>
                    <div class="row">
                        <div class="col-sm-12">
                            <span class="text-center servs">
                                <img src="page/img/icons/aire.svg" width="80px">
                                <h5>{!! trans('escalade.aire') !!}</h5>
                            </span>
                            <span class="text-center servs">
                                <img src="page/img/icons/asiento2.svg" width="80px">
                                <h5>{!! trans('escalade.asientos') !!}</h5>
                            </span>
                            <span class="text-center servs">
                                <img src="page/img/icons/tv1.svg" width="80px">
                                <h5>{!! trans('escalade.pantallas') !!}</h5>
                            </span>
                            <span class="text-center servs">
                                <img src="page/img/icons/gps.svg" width="80px">
                                <h5>{!! trans('escalade.geo') !!}</h5>
                            </span>
                            <span class="text-center servs">
                                <img src="page/img/icons/seguro.svg" width="80px">
                                <h5>{!! trans('escalade.seguro') !!}</h5>
                            </span>
                            <span class="text-center servs">
                                <img src="page/img/icons/velocidad.svg" width="80px">
                                <h5>{!! trans('escalade.velocidad') !!}</h5>
                            </span>
                        </div>
                    </div>

                </div>
            </div>
            <!--end of row-->
            <div class="row">
                <div class="col-sm-12">
                    <div class="image-slider slider-all-controls controls-inside">
                        <ul class="slides">
                            <li>
                                <img alt="Image" src="page/img/escalade/escalade1.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="page/img/escalade/escalade2.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="page/img/escalade/escalade3.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="page/img/escalade/escalade4.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="page/img/escalade/escalade5.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="page/img/escalade/escalade6.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="page/img/escalade/escalade7.png"/>
                            </li>

                        </ul>
                    </div>
                    <!--end of image slider-->
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section style="padding-bottom: 377px !important;padding-top: 270px !important;"
             class="pt240 pb240 parallax image-bg overlay bg-light">
        <div class="background-image-holder background-image-holder2">
            <img alt="image" class="background-image" src="page/img/escalade/int-escalade-3.jpg"/>
        </div>
        <div class="row">
            <div class="col-sm-12 text-right">
                <div style="margin-right:20%">
                    <h1 style="color:#fff;text-shadow: 0 0 30px rgba(0,0,0,.5);">{!! trans('escalade.interiores') !!}</h1>
                </div>
            </div>
        </div>
        <!--end of row-->
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="lightbox-grid square-thumbs" data-gallery-title="Gallery">
                        <ul>
                            <li>
                                <a href="page/img/escalade/int-escalade-1.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="page/img/escalade/int-escalade-1-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="page/img/escalade/int-escalade-2.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="page/img/escalade/int-escalade-2-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="page/img/escalade/int-escalade-3.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="page/img/escalade/int-escalade-3-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="page/img/escalade/int-escalade-4.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="page/img/escalade/int-escalade-4-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="page/img/escalade/int-escalade-5.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="page/img/escalade/int-escalade-5-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="page/img/escalade/int-escalade-6.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="page/img/escalade/int-escalade-6-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                             <li>
                                <a href="page/img/escalade/int-escalade-7.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="page/img/escalade/int-escalade-7-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                             <li>
                                <a href="page/img/escalade/int-escalade-8.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="page/img/escalade/int-escalade-8-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!--end of lightbox gallery-->
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection
