@extends('../layouts/master')

@section('content')
    <section class="page-title page-title-2 image-bg overlay parallax">
        <div class="background-image-holder">
            <img alt="Background Image" class="background-image" src="page/img/header-contacto.jpg"/>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="uppercase mb8">{{ trans('contacto.contacto') }}</h2>
                    <p class="lead mb0">{{ trans('contacto.sub-contacto') }}</p>
                </div>
                <div class="col-md-6 text-right">
                    <ol class="breadcrumb breadcrumb-2">
                        <li>
                            <a href="/">Inicio</a>
                        </li>
                        <li class="active">Contacto</li>
                    </ol>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section>
        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
            <div class="feature boxed bg-secondary">
                <form method="POST" action="/contacto" class="text-center">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <h4 class="uppercase mt48 mt-xs-0">{{ trans('contacto.h1formulario') }}</h4>
                    <p class="lead mb64 mb-xs-24">
                        {!!trans('contacto.sub-h1formulario')!!}
                    </p>
                    <div class="overflow-hidden">
                        <h6 class="uppercase">
                            {{ trans('contacto.instruccionesform') }}
                        </h6>
                        <input type="text" required="required" name="name" class="col-md-6 validate-required"
                               placeholder="{{ trans('contacto.nombre') }}"/>
                        <input type="text" required="required" name="email"
                               class="col-md-6 validate-required validate-email" placeholder="{{ trans('contacto.e-mail') }}"/>
                        <input type="text" required="required" name="phone" placeholder="{{ trans('contacto.tel') }}"/>
                        <textarea name="message_content" placeholder="{{ trans('contacto.mnsj') }}" rows="2"></textarea>
                        <hr>
                    </div>
                    <div class="overflow-hidden">
                        <div class="col-sm-6 col-sm-offset-3">
                            <h6 class="uppercase">
                                {{ trans('contacto.area') }}
                            </h6>
                            <div class="select-option">
                                <i class="ti-angle-down"></i>
                                <select name="referrer" required="required">
                                    <option selected value="Default">{{ trans('contacto.select') }}</option>
                                    <option value="website">{{ trans('contacto.op1') }}</option>
                                    <option value="friend">{{ trans('contacto.op2') }}</option>
                                    <option value="other">{{ trans('contacto.op3') }}</option>
                                    <option value="other">{{ trans('contacto.op4') }}</option>
                                </select>
                            </div>
                            <button type="submit">{{ trans('contacto.send') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3735.6028159320617!2d-100.37073454965146!3d20.563425786184887!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d344944f03d649%3A0x33fb87f986f78df2!2sTours+Ejecutivos+Oficina+Central!5e0!3m2!1ses-419!2smx!4v1479367711572"
            width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
@endsection
