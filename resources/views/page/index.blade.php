@extends('../layouts/master')

@section('content')
    {{--<div class="background-image-holder">
        <img alt="image" class="background-image" src="page/img/jetta/jetta.png"/>
    </div>
    <div class="container" style="margin:0 !important;">
        <div class="row">
            <div class="col-sm-12 text-left">
                <div style="margin-left:80px;">
                    <h1 style="color:#fff;text-shadow: 0 0 30px rgba(0,0,0,.5);">
                      {{ trans('index.intro') }}
                    </h1>
                    <a href="#inicio" class="btn btn-filled">{{ trans('index.vermasboton') }}</a>
                </div>
            </div>
        </div>
        <!--end of row-->
    </div>--}}
    <div class="container-fluid" style="margin:0 !important; padding: 0 !important; position: relative;">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            {{--<ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                <li data-target="#carousel-example-generic" data-slide-to="5"></li>
                <li data-target="#carousel-example-generic" data-slide-to="6"></li>
                <li data-target="#carousel-example-generic" data-slide-to="7"></li>
            </ol>--}}

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="page/img/carousel/auto-1.png" alt="...">
                </div>
                <div class="item">
                    <img src="page/img/carousel/auto-2.png" alt="...">
                </div>
                <div class="item">
                    <img src="page/img/carousel/camion-1.png" alt="...">
                </div>
                <div class="item">
                    <img src="page/img/carousel/international.jpg" alt="...">
                </div>
                <div class="item">
                    <img src="page/img/carousel/escalade.png" alt="...">
                </div>
                <div class="item">
                    <img src="page/img/carousel/crafter.jpg" alt="...">
                </div>
                <div class="item">
                    <img src="page/img/carousel/van-1.png" alt="...">
                </div>
                <div class="item">
                    <img src="page/img/carousel/van-2.png" alt="...">
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div style="position: absolute; right:12%;bottom:20%; text-align: right;">
            <h1 class="tituloh1" style="color:#fff;text-shadow: 0 0 30px rgba(0,0,0,.5);">{!! trans('index.intro') !!}
            <a href="#nosotros" class="btn btn-filled vermas">{{ trans('index.vermasboton') }}</a>
        </div>
    </div>
    <section id="inicio" class="pb0">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-12 text-center">
                    <h6 class="uppercase fade-half">{{ trans('index.pvehicular') }}</h6>
                    <h3 class="mb0">{{ trans('index.contamoscon') }}</h3>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section id="nosotros" class="projects">
        <div class="container">
            <div class="masonry-loader">
                <div class="col-sm-12 text-center">
                    <div class="spinner"></div>
                </div>
            </div>
            <div class="row masonry masonryFlyIn">
                <div class="col-sm-6 masonry-item project">
                    <div class="image-tile hover-tile text-center">
                        <img alt="image" class="background-image" src="page/img/carousel/auto-2.png"/>
                        <div class="hover-state">
                            <a href="/acerca-de-nosotros">
                                <h3 class="uppercase mb8">{{ trans('index.acercabutton') }}</h3>
                                <button class="btn btn-filled2">{{ trans('index.vermasboton') }}</button>
                            </a>
                        </div>
                    </div>
                    <!--end of hover tile-->
                </div>
                <div class="col-sm-6 masonry-item project">
                    <div class="image-tile hover-tile text-center">
                        <img alt="image" class="background-image" src="page/img/carousel/camion-1.png"/>
                        <div class="hover-state">
                            <a href="/galeria">
                                <h3 class="uppercase mb8">{{ trans('index.galeriaboton') }}</h3>
                                <button class="btn btn-filled2">{{ trans('index.vermasboton') }}</button>
                            </a>
                        </div>
                    </div>
                    <!--end of hover tile-->
                </div>
                <div class="col-sm-6 masonry-item project">
                    <div class="image-tile hover-tile text-center">
                        <img alt="image" class="background-image" src="{{ asset('page/img/carousel/international.jpg') }}"/>
                        <div class="hover-state">
                            <a href="/blog">
                                <h3 class="uppercase mb8">{{ trans('index.noticiasboton') }}</h3>
                                <button class="btn btn-filled2">{{ trans('index.vermasboton') }}</button>
                            </a>
                        </div>
                    </div>
                    <!--end of hover tile-->
                </div>
                <div class="col-sm-6 masonry-item project">
                    <div class="image-tile hover-tile text-center">
                        <img alt="image" class="background-image" src="page/img/carousel/escalade.png"/>
                        <div class="hover-state">
                            <a href="/contacto">
                                <h3 class="uppercase mb8">{{ trans('index.contactoboton') }}</h3>
                                <button class="btn btn-filled2">{{ trans('index.vermasboton') }}</button>
                            </a>
                        </div>
                    </div>
                    <!--end of hover tile-->
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="pt240 pb240 parallax image-bg overlay">
        <div class="background-image-holder">
            <img alt="image" class="background-image" src="page/img/escalade.png"/>
        </div>
        <h1 class="parallax-heading">{!! trans('index.parallaxinicio') !!}</h1>

    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-12 text-center mb80 mb-xs-24">
                    <h6 class="uppercase fade-half">{{ trans('index.somosexpertos') }}</h6>
                </div>
            </div>
            <!--end of row-->
            <div class="row">
                <div class="col-sm-4 text-center">
                    <div class="feature">
                        <i class="ti-cup icon fade-3-4 inline-block mb16"></i>
                        <h4>{{ trans('index.experiencia') }}</h4>
                        <p>
                            {{ trans('index.descripcionexperiencia') }}
                        </p>
                    </div>
                </div>
                <div class="col-sm-4 text-center">
                    <div class="feature">
                        <i class="ti-stats-up icon fade-3-4 inline-block mb16"></i>
                        <h4>{{ trans('index.conceptos') }}</h4>
                        <p>
                            {{ trans('index.descripcioncontextos') }}
                        </p>
                    </div>
                </div>
                <div class="col-sm-4 text-center">
                    <div class="feature">
                        <i class="ti-world icon fade-3-4 inline-block mb16"></i>
                        <h4>{{ trans('index.geoposicionamiento') }}</h4>
                        <p>
                            {{ trans('index.descripciongeoposicionamiento') }}
                        </p>
                    </div>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="bg-secondary">
        <div class="container">
            <div class="row v-align-children">
                <div class="col-md-7 col-sm-6 text-center mb-xs-24 overflow-hidden">
                    <div class="col-xs-6 p0">
                        <div class="hovereffect">
                            <img class="img-responsive" src="page/img/gps4.png" alt="">
                            <div class="overlay">
                                <i class="ti-search icon-sm"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 p0">
                        <div class="hovereffect">
                            <img class="img-responsive" src="page/img/gps1.png" alt="">
                            <div class="overlay">
                              <i class="ti-search icon-sm"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 p0">
                        <div class="hovereffect">
                            <img class="img-responsive" src="page/img/gps2.png" alt="">
                            <div class="overlay">
                                <i class="ti-search icon-sm"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 p0">
                        <div class="hovereffect">
                            <img class="img-responsive" src="page/img/gps3.png" alt="">
                            <div class="overlay">
                              <i class="ti-search icon-sm"></i>
                            </div>
                        </div>
                    </div>
                    <!--<div>
                        <img class="col-xs-6 p0" alt="Pic" src="img/gps4.png" />
                        <img class="col-xs-6 p0" alt="Pic" src="img/gps2.png" />
                        <img class="col-xs-6 p0" alt="Pic" src="img/gps3.png" />
                        <img class="col-xs-6 p0" alt="Pic" src="img/gps1.png" />
                    </div>-->
                </div>
                <div class="col-md-4 col-md-offset-1 col-sm-5 col-sm-offset-1 text-center">
                    <i class="ti-shield icon fade-3-4 inline-block mb16"></i>
                    <h3>{{ trans('index.seguridad') }}</h3>
                    <p>
                        {{ trans('index.descripcionseguridad') }}
                    </p>
                    <a class="btn" href="/galeria">{{ trans('index.vermasboton') }}</a>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection
