@extends('../layouts/master')
@section('styles')
    <style>
        .twitter-tweet-button{
            margin-top: 4px;
        }
    </style>
@endsection
@section('content')
    <section class="page-title page-title-4">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="uppercase mb0">{{ trans('blog.noticiastitle') }}</h3>
                </div>
                <div class="col-md-6 text-right">
                    <ol class="breadcrumb breadcrumb-2">
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li class="active">Galería de imágenes</li>
                    </ol>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="bg-secondary">
        <div class="container">
            <div class="row masonry-loader">
                <div class="col-sm-12 text-center">
                    <div class="spinner"></div>
                </div>
            </div>
            <div class="row masonry masonryFlyIn mb40">
                @if (count($publications) > 0)
                    @foreach($publications AS $publication)
                        @if ($publication->type == 'content')
                            <div class="col-sm-4 post-snippet masonry-item">
                                <a href="/blog/{{ $publication->slug }}">
                                    <img alt="{{ $publication->title }}"
                                         src="images/{{ $publication->creacion }}/{{ $publication->image }}"/>
                                </a>
                                <div class="inner">
                                    <a href="/blog/{{ $publication->slug }}">
                                        <h5 class="mb0">{{ $publication->title }}</h5>
                                        <span class="inline-block mb16">{{ $publication->mes }} {{ $publication->dia }}
                                            , {{ $publication->anio }}</span>
                                    </a>
                                    <hr>
                                    <a class="btn btn-sm" href="/blog/{{ $publication->slug }}">{{ trans('blog.readmore') }}</a>
                                    <ul class="tags pull-right">
                                        <li>
                                            <!--
                                              <a class="btn btn-sm btn-icon" href="#">
                                                  <i class="ti-twitter-alt"></i>
                                              </a>
                                            -->
                                            <a href="https://twitter.com/share" class="twitter-share-button"
                                               data-url="http://toursejecutivos.tk/blog/{{ $publication->slug }}"
                                               data-text="{{ $publication->title }}"
                                               data-via="musicinthehous3">Tweet</a>
                                        </li>
                                        <li>
                                            <div class="fb-share-button"
                                                 data-href="http://toursejecutivos.tk/blog/{{ $publication->slug }}"
                                                 data-layout="button" data-size="small" data-mobile-iframe="true">
                                                <a class="fb-xfbml-parse-ignore" target="_blank"
                                                   href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Ftoursejecutivos.tk%2F&amp;src=sdkpreparse">
                                                    <i class="ti-facebook"></i>
                                                </a>
                                            </div>
                                            <!--
                                            <a class="btn btn-sm btn-icon" href="#">
                                                <i class="ti-facebook"></i>
                                            </a>
                                            -->
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        @else
                            <div class="col-sm-4 post-snippet masonry-item">
                                <div class="embed-video-container embed-responsive embed-responsive-16by9 mb0"><?php echo html_entity_decode($publication->description); ?></div>
                                <!--end of embed video container-->
                                <div class="inner">
                                    <a href="http://blog.tours.dev:8080/blog/{{ $publication->slug }}">
                                        <h5 class="mb0">{{ $publication->title }}</h5>
                                        <span class="inline-block mb16">{{ $publication->mes }} {{ $publication->dia }}
                                            , {{ $publication->anio }}</span>
                                    </a>
                                    <hr>
                                    <a class="btn btn-sm" href="/blog/{{ $publication->slug }}">{{ trans('blog.readmore') }}</a>
                                    <ul class="tags pull-right">
                                        <li>
                                            <!--
                                            <a class="btn btn-sm btn-icon" href="#">
                                                <i class="ti-twitter-alt"></i>
                                            </a>-->
                                            <a href="https://twitter.com/share" class="twitter-share-button"
                                               data-url="http://blog.tours.dev:8080/blog/{{ $publication->slug }}"
                                               data-text="{{ $publication->title }}"
                                               data-via="musicinthehous3">Tweet</a>
                                        </li>
                                        <li>
                                            <div class="fb-share-button"
                                                 data-href="http://blog.tours.dev:8080/blog/{{ $publication->slug }}"
                                                 data-layout="button" data-size="small" data-mobile-iframe="true">
                                                <a class="fb-xfbml-parse-ignore" target="_blank"
                                                   href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fblog.tours.dev%2F&amp;src=sdkpreparse">
                                                    <i class="ti-facebook"></i>
                                                </a>
                                            </div>
                                            <!--
                                            <a class="btn btn-sm btn-icon" href="#">
                                                <i class="ti-facebook"></i>
                                            </a>
                                            -->
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        @endif


                    @endforeach
                @endif

            </div>
            <!--end of row-->
            <div class="row">
                <div class="text-center">
                {!! $publications->appends(Request::only(['search']))->render() !!}

                <!--
                            <ul class="pagination">
                                <li>
                                    <a href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="#">1</a>
                                </li>
                                <li>
                                    <a href="#">2</a>
                                </li>
                                <li>
                                    <a href="#">3</a>
                                </li>
                                <li>
                                    <a href="#">4</a>
                                </li>
                                <li>
                                    <a href="#">5</a>
                                </li>
                                <li>
                                    <a href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                </li>
                            </ul>
                            -->
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection
@section('scripts')


    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.8&appId=1439920832895088";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <script>
        !function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
            if (!d.getElementById(id)) {
                js = d.createElement(s);
                js.id = id;
                js.src = p + '://platform.twitter.com/widgets.js';
                fjs.parentNode.insertBefore(js, fjs);
            }
        }(document, 'script', 'twitter-wjs');
    </script>
@endsection
