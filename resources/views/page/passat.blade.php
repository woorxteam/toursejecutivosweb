@extends('../layouts/master')

@section('content')
    <section style="padding-bottom: 377px !important;padding-top: 270px !important;"
             class="pt240 pb240 parallax image-bg overlay bg-light">
        <div class="background-image-holder background-image-holder2">
            <img alt="image" class="background-image" src="/page/img/jetta/jetta.png"/>
        </div>
        <div class="row">
            <div class="col-sm-12 text-left">
                <div style="margin-left:20%">
                    <h1 style="color:#fff;text-shadow: 0 0 30px rgba(0,0,0,.5);">Passat</h1>
                </div>
            </div>
        </div>
        <!--end of row-->
    </section>
    <section>
        <div class="container pb0">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h4 class="uppercase mb16">{!! trans('passat.servicios') !!}</h4>
                    <div class="row">
                        <div class="col-sm-12">
                            <span class="text-center servs">
                                <img src="/page/img/icons/aire.svg" width="80px">
                                <h5>{!! trans('passat.aire') !!}</h5>
                            </span>
                            <span class="text-center servs">
                                <img src="/page/img/icons/asiento2.svg" width="80px">
                                <h5>{!! trans('passat.asientos') !!}</h5>
                            </span>
                            <span class="text-center servs">
                                <img src="/page/img/icons/gps.svg" width="80px">
                                <h5>{!! trans('passat.geo') !!}</h5>
                            </span>
                            <span class="text-center servs">
                                <img src="/page/img/icons/seguro.svg" width="80px">
                                <h5>{!! trans('passat.seguro') !!}</h5>
                            </span>
                            <span class="text-center servs">
                                <img src="/page/img/icons/velocidad.svg" width="80px">
                                <h5>{!! trans('passat.velocidad') !!}</h5>
                            </span>
                        </div>
                    </div>

                </div>
            </div>
            <!--end of row-->
            <div class="row">
                <div class="col-sm-12">
                    <div class="image-slider slider-all-controls controls-inside">
                        <ul class="slides">
                            <li>
                                <img alt="Image" src="/page/img/jetta/jetta1.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="/page/img/jetta/jetta2.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="/page/img/jetta/jetta3.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="/page/img/jetta/jetta4.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="/page/img/jetta/jetta5.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="/page/img/jetta/jetta6.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="/page/img/jetta/jetta7.png"/>
                            </li>

                        </ul>
                    </div>
                    <!--end of image slider-->
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section style="padding-bottom: 377px !important;padding-top: 270px !important;"
             class="pt240 pb240 parallax image-bg overlay bg-light">
        <div class="background-image-holder background-image-holder2">
            <img alt="image" class="background-image" src="/page/img/jetta/jetta-int1.jpg"/>
        </div>
        <div class="row">
            <div class="col-sm-12 text-right">
                <div style="margin-right:20%">
                    <h1 style="color:#fff;text-shadow: 0 0 30px rgba(0,0,0,.5);">{!! trans('passat.interiores') !!}</h1>
                </div>
            </div>
        </div>
        <!--end of row-->
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="lightbox-grid square-thumbs" data-gallery-title="Gallery">
                        <ul>
                            <li>
                                <a href="/page/img/jetta/jetta-int1.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image"
                                             src="/page/img/jetta/jetta-int1-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="/page/img/jetta/jetta-int2.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image"
                                             src="/page/img/jetta/jetta-int2-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="/page/img/jetta/jetta-int3.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image"
                                             src="/page/img/jetta/jetta-int3-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="/page/img/jetta/jetta-int4.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image"
                                             src="/page/img/jetta/jetta-int4-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="/page/img/jetta/jetta-int5.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image"
                                             src="/page/img/jetta/jetta-int5-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="/page/img/jetta/jetta-int6.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image"
                                             src="/page/img/jetta/jetta-int6-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="/page/img/jetta/jetta-int7.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image"
                                             src="/page/img/jetta/jetta-int7-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="/page/img/jetta/jetta-int8.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image"
                                             src="/page/img/jetta/jetta-int8-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!--end of lightbox gallery-->
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection
