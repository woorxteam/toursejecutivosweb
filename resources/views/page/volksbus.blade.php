@extends('../layouts/master')

@section('content')
    <section style="padding-bottom: 377px !important;padding-top: 270px !important;"
             class="pt240 pb240 parallax image-bg overlay bg-light">
        <div class="background-image-holder background-image-holder2">
            <img alt="image" class="background-image" src="page/img/volksbus/volksbus.png"/>
        </div>
        <div class="row">
            <div class="col-sm-12 text-right">
                <div style="margin-right:20%">
                    <h1 style="color:#fff;text-shadow: 0 0 30px rgba(0,0,0,.5);margin-right: -142px;">Man Bus and Truck VW</h1>
                </div>
            </div>
        </div>
        <!--end of row-->
    </section>
    <section>
        <div class="container pb0">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h4 class="uppercase mb16">{{trans('volksbus.servicios')}}</h4>
                    <div class="row">
                        <div class="col-sm-12">
                            <span class="text-center servs">
                                <img src="page/img/icons/aire.svg" width="80px">
                                <h5>{!! trans('volksbus.aire') !!}</h5>
                            </span>
                            <span class="text-center servs">
                                <img src="page/img/icons/asiento2.svg" width="80px">
                                <h5>{!! trans('volksbus.asientos') !!}</h5>
                            </span>
                            <span class="text-center servs">
                                <img src="page/img/icons/tv1.svg" width="80px">
                                <h5>{!! trans('volksbus.pantallas') !!}</h5>
                            </span>
                            <span class="text-center servs">
                                <img src="page/img/icons/gps.svg" width="80px">
                                <h5>{!! trans('volksbus.geo') !!}</h5>
                            </span>
                            <span class="text-center servs">
                                <img src="page/img/icons/seguro.svg" width="80px">
                                <h5>{!! trans('volksbus.seguro') !!}</h5>
                            </span>
                            <span class="text-center servs">
                                <img src="page/img/icons/velocidad.svg" width="80px">
                                <h5>{!! trans('volksbus.velocidad') !!}</h5>
                            </span>
                        </div>
                    </div>

                </div>
            </div>
            <!--end of row-->
            <div class="row">
                <div class="col-sm-12">
                    <div class="image-slider slider-all-controls controls-inside">
                        <ul class="slides">
                            <li>
                                <img alt="Image" src="page/img/volksbus/volksbus1.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="page/img/volksbus/volksbus2.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="page/img/volksbus/volksbus3.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="page/img/volksbus/volksbus4.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="page/img/volksbus/volksbus5.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="page/img/volksbus/volksbus6.png"/>
                            </li>
                            <li>
                                <img alt="Image" src="page/img/volksbus/volksbus7.png"/>
                            </li>

                        </ul>
                    </div>
                    <!--end of image slider-->
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section style="padding-bottom: 377px !important;padding-top: 270px !important;"
             class="pt240 pb240 parallax image-bg overlay bg-light">
        <div class="background-image-holder background-image-holder2">
            <img alt="image" class="background-image" src="page/img/volksbus/volksbus-int4.jpg"/>
        </div>
        <div class="row">
            <div class="col-sm-12 text-right">
                <div style="margin-right:20%">
                    <h1 style="color:#fff;text-shadow: 0 0 30px rgba(0,0,0,.5);">Interiores</h1>
                </div>
            </div>
        </div>
        <!--end of row-->
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="lightbox-grid square-thumbs" data-gallery-title="Gallery">
                        <ul>
                            <li>
                                <a href="page/img/volksbus/volksbus-int1.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="page/img/volksbus/volksbus-int1-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="page/img/volksbus/volksbus-int2.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="page/img/volksbus/volksbus-int2-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="page/img/volksbus/volksbus-int3.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="page/img/volksbus/volksbus-int3-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="page/img/volksbus/volksbus-int4.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="page/img/volksbus/volksbus-int4-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="page/img/volksbus/interior-volksbus-5.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="page/img/volksbus/interior-volksbus-5-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="page/img/volksbus/interior-volksbus-6.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="page/img/volksbus/interior-volksbus-6-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="page/img/volksbus/interior-volksbus-7.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="page/img/volksbus/interior-volksbus-7-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="page/img/volksbus/interior-volksbus-8.jpg" data-lightbox="true">
                                    <div class="background-image-holder">
                                        <img alt="image" class="background-image" src="page/img/volksbus/interior-volksbus-8-thumb.jpg"/>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!--end of lightbox gallery-->
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection
