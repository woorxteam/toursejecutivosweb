@extends('../layouts/master')

@section('content')
    <section class="page-title page-title-4 bg-secondary">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="uppercase mb0">
                        <i class="ti-gallery"></i> Galería de imagenes</h3>
                </div>
                <div class="col-md-6 text-right">
                    <ol class="breadcrumb breadcrumb-2">
                        <li>
                            <a href="/">Inicio</a>
                        </li>
                        <li>
                            <a href="#">Nosotros</a>
                        </li>
                        <li class="active">Galería</li>
                    </ol>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <!--
                      <h4 class="uppercase mb16">Lightbox Grid Gallery</h4>
                      <p class="lead mb64">
                          A simple lightbox grid with square thumbnails.
                      </p>
                    -->
                </div>
            </div>
            <!--end of row-->
            <div class="row">
                <div class="col-sm-12">
                    <div class="lightbox-grid square-thumbs">
                        <ul>
                            @if(count($albums) > 0)
                                @foreach($albums AS $album)
                                    <li>
                                        <a href="/galeria/{{ $album->id }}">
                                            <div class="background-image-holder">
                                                <img alt="image" class="background-image" src="/images/{{ $album->creacion }}/{{ $album->image }}"/>
                                            </div>
                                        </a>
                                        <h4 class="category-title">{{ $album->title }}</h4>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                    <!--end of lightbox gallery-->
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection
