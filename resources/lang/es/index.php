<?php

return [

    /*
    Sección de inicio
    */
    'vermasboton' => 'Ver más',
    'intro' => 'Líderes en transporte <br/> de personal <br /><br />',
    'pvehicular' => 'Contámos con Parque Vehicular',
    'contamoscon' => 'Actualmente realizamos el traslado de 45,000 Mil personas diariamente.',

    #Botones
    'acercabutton' => 'Acerca de nosotros',
    'galeriaboton' => 'Galería de imagenes',
    'noticiasboton' => 'Noticias',
    'contactoboton' => 'Contacto',

    #Parallax inicio
    'parallaxinicio' => 'Servicio ejecutivo <br />de transporte.',

    #Secciones pre footer
    'somosexpertos' => 'Sómos Expertos',
    'experiencia' => 'Experiencia',
    'descripcionexperiencia' => 'Más de 27 años de experiencia en el mercado de transporte especializado de personal de primer nivel.',
    'conceptos' => 'Conceptos',
    'descripcioncontextos' => 'Nuestra filosofía y conceptos nos mantienen a la vanguardía y nos coloca como lideres a nivel nacional.',
    'geoposicionamiento' => 'Geoposicionamiento',
    'descripciongeoposicionamiento' => 'Contamos con tecnología de control y rastreo de alta fiabilidad en cada una de nuestras rutas.',
    'seguridad' => 'Seguridad 24/7',
    'descripcionseguridad' => 'Cada unidad cuenta con asistencia, control y rastreo las veinticuatro horas, los 7 días de la semana desde nuestro centro de operaciones y control geosatelital.',
];
