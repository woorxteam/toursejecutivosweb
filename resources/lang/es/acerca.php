<?php

return [

    /*
    Sección de Quiénes somos
    */
    'titulo-nosotros' => 'Nuestra Historia',
    'descripcion-nosotros' => 'Movilidad con sentido humano',

    #bloque acerca de nosotros
    'quienes-somos' => '¿Quiénes somos?',
    'acerca-de-nosotros' => 'Una empresa Queretana con más de 30 años de experiencia en el ramo del transporte. Dedicada al servicio de transporte especializado de primer nivel, con personal altamente capacitado. Debido a la demanda que existe en la Industria Queretana y del Bajío por encontrar un servicio de transporte de personal. Tours Ejecutivos surge como una de las mejores alternativas. Ofrecemos una gran variedad de servicios de autotransporte de personal y ejecutivo de acuerdo a las necesidades para hacer de los viajes de nuestros clientes una experiencia inigualable. <br /> En el 2014 y 2015 se tuvo un crecimiento importante de un 47.7% en unidades y 11.3% en clientes.<bR />Actualmente nuestro parque vehicular cuenta con más de 260 unidades entre camiones (VW, INTERNATIONAL, y MERCEDES-BENZ) camionetas (URVAN y CRAFTER) y autos (JETTAS, TIIDAS y PASSAT), con nuevos clientes de clase mundial como SIEMENS, MALHE, AUTOLIV CMX/AQW Y SAMSUMG.',
    'mision-titulo' => 'Misión',
    'mision' => 'Brindar soluciones personalizadas en Transporte de Personal ofreciendo un servicio incluyente y profesional caracterizado por la excelencia en seguridad, calidad, comodidad y puntualidad, con el compromiso de establecer una sólida relación de confianza con nuestros clientes asegurando la satisfacción total del usuario.',
    'vision-titulo' => 'Visión',
    'vision' => 'Ser la empresa líder en el ramo de Transporte de Personal, reconocida por ofrecer un servicio de vanguardia que traduce las necesidades de sus clientes en soluciones integrales de movilidad a través del desarrollo profesional de sus colaboradores.',
    'pdecalidad-titulo' => 'Política de Calidad',
    'pdecalidad' => 'Estamos comprometidos en satisfacer las necesidades del Transporte de nuestros clientes, ofreciendo un valor agregado que supere sus expectativas, promoviendo permanentemente la innovación y la mejora continua. Garantizar la seguridad laboral, preservar el medio ambiente y cumplir con los requisitos regulatorios aplicables.',

    #valores
    'puntualidad' => 'Puntualidad',
    'atencion' => 'Atención al cliente',
    'inovación' => 'Innovación',
    'mejora' => 'Mejora continua',
    'confiabilidad' => 'Confiabilidad',
    'seguridad' => 'Seguridad',

    #Prefooter
    'Certificaciones' => 'Certificaciones',
    'nuestrosclientes' => 'Nuestros Clientes',
    'desc-nuestrosclientes' => 'Cada cliente es más experiencia, ellos han depositado su confianza en nosotros.',
    'instalaciones' => 'Contamos con las mejores instalaciones',
    'desc-instalaciones' => 'Especialistas monitoreando las 24 horas del día, con rastreo GPS, la seguridad es nuestra prioridad.',
    'conoce' => 'Conoce nuestras instalaciones',
];
