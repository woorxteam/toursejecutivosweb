<?php

return [

    /*
    Tiida
    */

  'servicios' => 'Servicios',
  'aire' => 'aire acondicionado',
  'asientos' => '8 asientos',
  'pantallas' => 'pantallas',
  'geo' => 'geolocalización',
  'seguro' => 'seguro de viajero',
  'velocidad' => 'velocidad regulada',
  'interiores' => 'Interiores',
];
