<?php

return [
  'contacto' => 'Contáctenos',
  'sub-contacto' => 'Estámos para servirle, proporcione la siguiente información para ponernos en contacto con usted.',
  'h1formulario' => 'QUERÉMOS ESCUCHARLE Y ESTAR EN CONTACTO CON USTED.',
  'sub-h1formulario' => 'Nuestro equipo se pondrá en contacto lo antes posible.',
  'instruccionesform' => 'INGRESE SUS DATOS DE CONTACTO',
  'nombre' => 'Nombre Completo',
  'e-mail' => 'E-Mail',
  'tel' => 'Teléfono',
  'mnsj' => 'Mensaje',
  'area' => '¿A que area desea comunicarse?',
  'select' => 'Seleccione una opción:',
  'op1' => 'Dirección General',
  'op2' => 'Administración',
  'op3' => 'Bolsa de trabajo',
  'op4' => 'Contacto',
  'send' => 'Enviar',
];
