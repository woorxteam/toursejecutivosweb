<?php

return [
    'inicio' => 'Inicio',
    'nosotros' => 'Nosotros',
    'modelos' => 'Modelos',
    'galeria' => 'Galería',
    'blog' => 'Blog',
    'contacto' => 'Contacto',
    'mapa' => 'Mapa del sitio',
    'lang' => 'Idioma',
];
