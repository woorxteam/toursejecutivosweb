<?php

return [

    /*
    Tiida
    */

    'servicios' => 'services',
    'aire' => 'air conditioner',
    'asientos' => '8 seats',
    'pantallas' => 'screen',
    'geo' => 'geolocation',
    'seguro' => 'travel ensurance',
    'velocidad' => 'regulated speed',
    'interiores' => 'Interiors',
];
