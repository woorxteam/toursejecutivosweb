<?php

return [

    /*
    Tiida
    */

    'servicios' => 'services',
    'aire' => 'air conditioner',
    'asientos' => '41 seats',
    'pantallas' => 'led screen',
    'geo' => 'geolocation',
    'seguro' => 'travel ensurance',
    'velocidad' => 'regulated speed',
    'interiores' => 'Interiors',
];
