<?php

return [

    /*
    Sección de inicio
    */
    'vermasboton' => 'More',
    'intro' => 'Leaders in the <br/>transportation of personnel <br /><br />',
    'pvehicular' => 'Vehicle Fleet',
    'contamoscon' => 'We currently transport 45,000 people daily',

    #Botones
    'acercabutton' => 'About us',
    'galeriaboton' => 'Photo gallery',
    'noticiasboton' => 'News',
    'contactoboton' => 'Contact',

    #Parallax inicio
    'parallaxinicio' => 'Executive transportation <br />service',

    #Secciones pre footer
    'somosexpertos' => 'We are the experts in transportation',
    'experiencia' => 'Experience',
    'descripcionexperiencia' => 'More than 30 years of experience in the industry, our company specializes in the safe transportation of personnel. TOURS EJECUTIVOS Transporte de Personal is considered the best choice in first class transportation.',
    'conceptos' => 'Philosophy',
    'descripcioncontextos' => 'Our Quality and Values ​​Policy encourage us to remain committed to meeting the transportation needs of our customers, offering an added value that exceeds their expectations, permanently promoting innovation. Ensure work safety, preserve the environment and comply with applicable regulatory requirements.',
    'geoposicionamiento' => 'Geo positioning',
    'descripciongeoposicionamiento' => 'We apply with first class control technology and highly reliable tracking mechanism in each and every one of our vehicles.',
    'seguridad' => 'Security 24/7',
    'descripcionseguridad' => 'Each unit has 24 hour / 7 day per week assistance and tracking from our geo satellite control and operations center.',
];
