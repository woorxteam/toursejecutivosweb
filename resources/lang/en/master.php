<?php

return [
    'inicio' => 'Main',
    'nosotros' => 'About us',
    'modelos' => 'Models',
    'galeria' => 'Gallery',
    'blog' => 'Blog',
    'contacto' => 'Contact',
    'mapa' => 'Site map',
    'lang' => 'Language',
    ];
