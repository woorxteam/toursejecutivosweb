<?php

return [

    /*
    Sección de Quiénes somos
    */
    'titulo-nosotros' => 'Our history',
    'descripcion-nosotros' => 'A vehicle with human sense',

    #bloque acerca de nosotros
    'quienes-somos' => 'About us',
    'acerca-de-nosotros' => 'A company from Queretaro City with more than 30 years of experience in the field of transportation. Dedicated to providing specialized first class transportation services, with highly qualified personnel. Due to the demand that exists in Querétaro and Bajío Industry for finding quality transportation service, Tours Ejecutivos offers the best choice. We provide a great variety of transportation services that fit the needs of our customers, we strive to make our clients travel experience unparalleled.
     In 2014 and 2015 there was a significant growth of 50% in vehicles and 15% in customers.
     Currently our vehicle fleet has more than 260 units between trucks (VW, INTERNATIONAL, and MERCEDES-BENZ), van (URVAN and CRAFTER), cars (JETTAS, TIIDAS and PASSAT), with new world-class customers like SIEMENS, MALHE, AUTOLIV CMX / AQW and SAMSUNG.',
    'mision-titulo' => 'Mission',
    'mision' => 'Provide personalized solutions in the transportation of personnel offering an inclusive and professional service characterized by excellence in safety, quality, comfort and punctuality, with the commitment to establish a solid relationship of trust with our clients ensuring total client satisfaction.',
    'vision-titulo' => 'Vision',
    'vision' => 'To be the leader in the field of Personnel Transportation, known for offering a state of art service that translates the needs of its customers into comprehensive mobility solutions through the professional development of its employees.',
    'pdecalidad-titulo' => 'Quality politics',
    'pdecalidad' => 'We are committed to meeting the needs of our customers transportation, offering an added value that exceeds their expectations, permanently promoting innovation and continuous improvement. Ensuring work safety, preserving the environment and complying with applicable regulatory requirements',

    #valores
    'puntualidad' => 'Punctuality',
    'atencion' => 'Customer Support',
    'inovación' => 'Innovation',
    'mejora' => 'Leadership',
    'confiabilidad' => 'Confidence',
    'seguridad' => 'Security',

    #Prefooter
    'Certificaciones' => 'Awards and certifications',
    'nuestrosclientes' => 'Our clients',
    'desc-nuestrosclientes' => 'Every customer that has experienced our services, have placed their trust in us.',
    'instalaciones' => 'We have the best facilities',
    'desc-instalaciones' => 'Equipment is monitored 24 hours a day, with GPS tracking, safety is our priority.',
    'conoce' => 'Visit our facilities',
];
