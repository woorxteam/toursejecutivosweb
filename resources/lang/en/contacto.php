<?php

return [
  'contacto' => 'Contact us',
  'sub-contacto' => 'We are here to serve you, please type us your information',
  'h1formulario' => 'WE WANT TO LISTEN TO YOU, WE WILL CONTACT YOU.',
  'sub-h1formulario' => 'Our team is responsible for getting in touch with you.',
  'instruccionesform' => 'PLEASE WRITE YOUR CONTACT DETAILS',
  'nombre' => 'Full Name',
  'e-mail' => 'E-Mail',
  'tel' => 'Telephone',
  'mnsj' => 'Message',
  'area' => 'What are you wanting to communicate?',
  'select' => 'Please select an option:',
  'op1' => 'General Direction',
  'op2' => 'Administration',
  'op3' => 'Employment exchange',
  'op4' => 'Sales',
  'send' => 'Send',
];
