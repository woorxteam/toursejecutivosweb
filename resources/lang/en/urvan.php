<?php

return [

    /*
    Urvan
    */

    'servicios' => 'services',
    'aire' => 'air conditioner',
    'asientos' => '14 seats',
    'pantallas' => 'screen',
    'geo' => 'geolocation',
    'seguro' => 'travel ensurance',
    'velocidad' => 'regulated speed',
    'interiores' => 'Interiors',
];
